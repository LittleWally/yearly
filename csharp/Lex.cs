using System;
/// Plural lexical analyser
namespace Yearly {
    public class Lex {
        private int ptr;

        public static string[] split(string arg) {
            return new Lex().processString(arg);
        }

        private string[] processString(string arg0) {
            int c;
            string arg;

            if (arg0.Length > 1 && arg0.Substring(0, 2) == "//") {
                return new string[] { "\n" };
            };
            
            c = quoted(arg0).IndexOf("//"); // trailing comments
            arg = (c != -1) ? arg0.Substring(0, c) : arg0;

            string[] r = new string[arg.Length + 1];

            int d;
            string s;
            bool n0, n1;
            d = 0;
            arg = ' ' + arg + '\n';
            ptr = 1;
            n0 = false;

            while (ptr < arg.Length) {
                c = ptr;
                s = arg.Mid(c, 1);
                n1 = false;
                switch (s) {
                    case " ":
                        ptr = c + 1;
                        s = "";
                        break;
                    case "/":
                        s = comment(arg);
                        if (s.Length == 0) s = fun(arg);
                        break;
                    case "\"":
                        s = quote(arg, "\"");
                        break;
                    case "'":
                        s = quote(arg, "'");
                        break;
                    case "_":
                    case "a":
                    case "b":
                    case "c":
                    case "d":
                    case "e":
                    case "f":
                    case "g":
                    case "h":
                    case "i":
                    case "j":
                    case "k":
                    case "l":
                    case "m":
                    case "n":
                    case "o":
                    case "p":
                    case "q":
                    case "r":
                    case "s":
                    case "t":
                    case "u":
                    case "v":
                    case "w":
                    case "x":
                    case "y":
                    case "z":
                    case "A":
                    case "B":
                    case "C":
                    case "D":
                    case "E":
                    case "F":
                    case "G":
                    case "H":
                    case "I":
                    case "J":
                    case "K":
                    case "L":
                    case "M":
                    case "N":
                    case "O":
                    case "P":
                    case "Q":
                    case "R":
                    case "S":
                    case "T":
                    case "U":
                    case "V":
                    case "W":
                    case "X":
                    case "Y":
                    case "Z":
                        s = alpha(arg);
                        break;
                    case "-":
                        n1 = true;
                        s = num2(arg);
                        if (s.Length == 0) {
                            s = num1(arg);
                        }
                        if (s.Length == 0) {
                            s = fun(arg);
                            n1 = false;
                        }
                        break;
                    case "0":
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9":
                        n1 = true;
                        s = num2(arg);
                        if (s.Length == 0) {
                            s = num1(arg);
                        }
                        break;
                    case "#":
                    case "!":
                    case "|":
                    case "?":
                    case "+":
                    case "*":
                    case "%":
                    case "\\":
                    case "<":
                    case ">":
                    case "=":
                    case "~":
                    case ".":
                        s = fun(arg);
                        break;
                    case ":":
                        s = asgn(arg);
                        break;
                    case "(":
                    case ")":
                    case "[":
                    case "]":
                    case "{":
                    case "}":
                        s = arg.Mid(c, 1);
                        ptr = c + 1;
                        break;
                    case ";":
                    case "&":
                    case "`":
                    case "´":
                    case ",":
                    case "¨":
                        s = arg.Mid(c, 1);
                        ptr = c + 1;
                        break;


                    default:
                        break;
                }

                if ("" != s) {
                    if (n0 && n1) {  // not here
                        r[d - 1] = r[d - 1] + " " + s;
                    } else {
                        r[d] = s;
                        d = d + 1;
                    }
                    // r[d++] = s; // when not combining numeric tokens

                    n0 = n1;
                }
            }

            r[d] = "\n";
            return r;
        }

        private string quote(string arg, string k) {
            int b, c;
            bool qt;
            qt = true;
            b = ptr;
            c = b + 1;

            while (qt || arg.Mid(c, 1) == k) {
                if (arg.Mid(c, 1) == k) qt = !qt;
                c = c + 1;
            }
            ptr = c;
            string r = arg.Mid(b, c - b);
            return r;
        }

        private string alpha(string arg) {
            int b = ptr;
            int c = b;
            int d = arg[c - 1];

            while (d == 95 || (d >= 97 && d <= 122) || (d >= 65 && d <= 90) || (d >= 48 && d <= 57)) {
                c = c + 1;
                d = arg[c - 1];
            }
            ptr = c;
            return arg.Mid(b, c - b);
        }

        private string fun(string arg) {
            int c = ptr;
            int d = 1;

            char s1 = arg[c - 1];
            char s2 = arg[c];
            char s3 = arg[c + 1];
            string r = "" + s1;

            switch (s1) {
                case '+':
                case '-':
                    switch (s2) {
                        case '#':
                            d = 2;
                            break;
                        case '!':
                            d = 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case '*':
                    switch (s2) {
                        case '*':
                            d = 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case '#':
                    switch (s2) {
                        case '-':
                        case '+':
                            d = 2;
                            switch (s3) {
                                case '#':
                                    d = 3;
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case '!':
                    switch (s2) {
                        case '-':
                        case '+':
                            d = 2;
                            switch (s3) {
                                case '!':
                                    d = 3;
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case '%':
                case '/':
                case '\\':
                    break;
                case '<':
                    switch (s2) {
                        case '=':
                            d = 2;
                            break;
                        case '>':
                            d = 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case '>':
                    switch (s2) {
                        case '=':
                            d = 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case '|':
                    switch (s2) {
                        case '|':
                            d = 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case '=':
                case '~':
                    switch (s2) {
                        case '=':
                        case '&':
                        case '|':
                            d = 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case '.':
                    switch (s2) {
                        case '.':
                            d = 2;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

            if (d == 2) {
                r = "" + s1 + s2;
            }
            else if (d == 3) {
                r = "" + s1 + s2 + s3;
            }
            ptr = c + d;
            return r;
        }

        private string asgn(string arg) {
            int c = ptr;
            int d = 1;
            char s1 = arg[c - 1];
            char s2 = arg[c];
            string r;

            r = "" + s1;

            switch (s1) {
                case ':':
                    switch (s2) {
                        case '=':
                            d = 2;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            if (d == 2) r = "" + s1 + s2;

            ptr = c + d;
            return r;
        }

        private string comment(string arg) {

            int c, d;
            string r;
            r = "";
            c = ptr;
            if (arg.Mid(c + 1, 1) != "/") {
                c = c + 0;
                return r;
            }

            d = arg.Length - 1;
            r = arg.Mid(c, d - c + 1);
            ptr = d + 1;
            return r;
        }

        private string num1(string arg) {

            int i, l, c, s, p, r;

            l = arg.Length;
            p = ptr;
            s = ptr;
            r = 0;
            string num1 = "";

            c = arg[p - 1];
            if (c == 45) {
                p++;
                r++;
            }

            c = arg[p - 1];
            if (c < 48 || c > 57) return num1;
            p++;
            r++;

            for (i = p; i <= l; i++) {
                c = arg[i - 1];
                if (c < 48 || c > 57) break;
                p++;
                r++;
            }

            num1 = arg.Mid(s, p - s);
            ptr = s + r;

            if (p > l) return num1;

            c = arg[p - 1];
            if (c != 46) return num1;
            p++;
            r++;

            if (p > l) return num1;

            c = arg[p - 1];
            if (c < 48 || c > 57) return num1;
            p++;
            r++;

            for (i = p; i <= l; i++) {
                c = arg[i - 1];
                if (c < 48 || c > 57) break;
                p++;
                r++;
            }

            num1 = arg.Mid(s, p - s);
            ptr = s + r;
            return num1;
        }

        private string num2(string arg) {

            int i, l, c, s, p, r;
            l = arg.Length;
            p = ptr;
            s = ptr;
            r = 0;
            string num2 = "";

            c = arg.Mid(p, 1)[0];
            if (c == 45) {
                p++;
                r++;
            }

            c = arg.Mid(p, 1)[0];
            if (c < 48 || c > 57) return num2;
            p++;
            r++;

            for (i = p; i <= l; i++) {
                c = arg.Mid(i, 1)[0];
                if (c < 48 || c > 57) break;
                p++;
                r++;
            }

            if (p > l) return num2;

            c = arg.Mid(p, 1)[0];
            if (c == 46) {
                p++;
                r++;
            }

            if (p > l) return num2;

            c = arg.Mid(p, 1)[0];
            if (c < 48 || c > 57) return num2;
            p++;
            r++;

            for (i = p; i <= l; i++) {
                c = arg.Mid(i, 1)[0];
                if (c < 48 || c > 57) break;
                p++;
                r++;
            }

            if (p > l) return num2;

            c = arg.Mid(p, 1)[0];
            if (c != 101) return num2;
            p++;
            r++;

            c = arg.Mid(p, 1)[0];
            if (c == 45) {
                p++;
                r++;
            }

            if (p > l) return num2;

            c = arg.Mid(p, 1)[0];
            if (c < 48 || c > 57) return num2;
            p++;
            r++;

            for (i = p; i == l; i++) {
                c = arg.Mid(i, 1)[0];
                if (c < 48 || c > 57) break;
                p++;
                r++;
            }

            num2 = arg.Mid(s, p - s);
            ptr = s + r;
            return num2;
        }

        private string date(string arg) {
            if (arg.Length < ptr + 10) return "";
            if (arg.Mid(ptr + 4, 1) != ".") return "";
            if (arg.Mid(ptr + 7, 1) != ".") return "";
            int d;
            int e = (arg[ptr] - 48) * 10000000;

            d = arg[ptr + 1];
            if (d < 48 || d > 57) return "";
            e += (d - 48) * 1000000;

            d = arg[ptr + 2];
            if (d < 48 || d > 57) return "";
            e += (d - 48) * 100000;

            d = arg[ptr + 3];
            if (d < 48 || d > 57) return "";
            e += (d - 48) * 10000;

            d = arg[ptr + 5];
            if (d < 48 || d > 57) return "";
            e += (d - 48) * 1000;

            d = arg[ptr + 6];
            if (d < 48 || d > 57) return "";
            e += (d - 48) * 100;

            d = arg[ptr + 8];
            if (d < 48 || d > 57) return "";
            e += (d - 48) * 10;

            d = arg[ptr + 9];
            if (d < 48 || d > 57) return "";
            e += d - 48;

            if (!Duration.isDate(e)) return "";
            string r = arg.Mid(ptr, ptr + 10);
            ptr += 10;
            return r;
        }

        public string quoted(string arg) {
            char[] r = new char[arg.Length];
            bool qt = false;
            for (int i = 0; i < arg.Length; i++) {
                if (arg[i] == '\"') qt = !qt;
                r[i] = qt ? '\"' : arg[i];
            }
            return new string(r);
        }
    }
}

