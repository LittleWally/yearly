﻿using System;

namespace Yearly {
    public class ChangeFrequency {

        public static TimeSeries compressAnnual(TimeSeries arg, string fn) {
            return compress1(arg, fn, 1);
        }

        public static TimeSeries compressQuarterly(TimeSeries arg, string fn) {
            return compress1(arg, fn, 4);
        }

        public static TimeSeries compressWeekly(TimeSeries arg, string fn) {
            return compress1(arg, fn, 53);
        }

        public static TimeSeries compressMonthly(TimeSeries arg, string fn) {
            return compress1(arg, fn, 12);
        }

        public static TimeSeries expandDaily(TimeSeries arg, string fn) {
            return expand1(arg, fn, 366);
        }

        public static TimeSeries expandQuarterly(TimeSeries arg, string fn) {
            return expand1(arg, fn, 4);
        }

        public static TimeSeries expandWeekly(TimeSeries arg, string fn) {
            return expand1(arg, fn, 53);
        }

        public static TimeSeries expandMonthly(TimeSeries arg, string fn) {
            return expand1(arg, fn, 12);
        }

        private static TimeSeries compress1(TimeSeries arg, string fn, int freq) {
            TimeSeries result = arg.tsCopy();
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] b = arg.ts[i];
                double a = b[0];
                int f = Duration.freq(a);
                if (f <= freq) throw new Exception("Incorrect frequency " + f);
                if (f > 12) {
                    b = expandToDaily(b, fn);
                    a = b[0];
                    f = 366;
                }
                int[] r = new int[b.Length];
                r[0] = (int)b[0];
                int[] r0 = (int[])r.Clone();

                int p = Duration.period(a);
                int d = Duration.periodDate(p, f);
                int e = Duration.periodNr(d, f);
                for (int j = 1; j < r.Length; j++) {
                    int h = Duration.periodDate(e, f);
                    r0[j] = e++;
                    r[j] = Duration.periodNr(h, freq);
                }
                p = r[1];
                double[] r2 = new double[r[r.Length - 1] - p + 2];
                double[] r3;
                p--;

                // sum max min last mean avg amean gmean hmean
                switch (fn) {
                    case "sum":
                        for (int j = 0; j < r2.Length; j++) r2[j] = 0.0;
                        for (int j = 1; j < r.Length; j++) {
                            r2[r[j] - p] += b[j];
                        }
                        break;
                    case "max":
                        for (int j = 0; j < r2.Length; j++) r2[j] = double.MinValue;
                        for (int j = 1; j < r.Length; j++) {
                            r2[r[j] - p] = Math.Max(b[j], r2[r[j] - p]);
                        }
                        break;
                    case "min":
                        for (int j = 0; j < r2.Length; j++) r2[j] = double.MaxValue;
                        for (int j = 1; j < r.Length; j++) {
                            r2[r[j] - p] = Math.Min(b[j], r2[r[j] - p]);
                        }
                        break;
                    case "last":
                        for (int j = 1; j < r.Length; j++) {
                            r2[r[j] - p] = b[j];
                        }
                        break;
                    case "mean":
                    case "avg":
                    case "amean":
                        for (int j = 0; j < r2.Length; j++) r2[j] = 0.0;
                        r3 = (double[])r2.Clone();
                        for (int j = 1; j < r.Length; j++) {
                            r2[r[j] - p] += b[j];
                            r3[r[j] - p]++;
                        }
                        for (int j = 1; j < r2.Length; j++) {
                            r2[j] /= r3[j];
                        }
                        break;
                    case "gmean":
                        for (int j = 0; j < r2.Length; j++) r2[j] = 1.0;
                        r3 = (double[])r2.Clone();
                        for (int j = 1; j < r.Length; j++) {
                            r2[r[j] - p] *= b[j];
                            r3[r[j] - p]++;
                        }
                        for (int j = 1; j < r2.Length; j++) {
                            r2[j] = Math.Pow(r2[j], 1 / r3[j]);
                        }
                        break;
                    case "hmean":
                        for (int j = 0; j < r2.Length; j++) r2[j] = 0.0;
                        r3 = (double[])r2.Clone();
                        for (int j = 1; j < r.Length; j++) {
                            r2[r[j] - p] += 1.0 / b[j];
                            r3[r[j] - p]++;
                        }
                        for (int j = 1; j < r2.Length; j++) {
                            r2[j] = r3[j] / r2[j];
                        }
                        break;
                    default:
                        throw new Exception("Operator Argument Syntax Error " + fn);
                }
                r2[0] = r[0] + 0.001 * freq;

                int d1, d2;
                d1 = Duration.periodDate(r[1], freq);
                d2 = Duration.periodDate(r0[1], f);
                if (d1 != d2) r2[1] = Double.NaN;

                d1 = Duration.periodDateEnd(r[r.Length - 1], freq);
                d2 = Duration.periodDateEnd(r0[r0.Length - 1], f);
                if (d1 != d2) r2[r2.Length - 1] = Double.NaN;

                result.ts[i] = r2;
            }
            return result;
        }

        private static double[] expandToDaily(double[] arg, string fn) {
            int[]
            t = new int[arg.Length];
            double a = arg[0];
            int f = Duration.freq(a);
            int p = Duration.period(a);
            int d = Duration.periodDate(p, f);
            int e = Duration.dayNumber(d);
            int s = 0;
            for (int j = 1; j < t.Length; j++) {
                int h = Duration.periodDate(p + j, f);
                int g = Duration.dayNumber(h);
                t[j] = g - e;
                s += t[j];
                e = g;
            }

            double[] r = new double[s + 1];
            string fn2 = companionFn(fn);

            int c = 1;
            double x = 0.0;
            switch (fn2) {
                case "step":
                    for (int i = 1; i < t.Length; i++) {
                        x = arg[i] / t[i];
                        for (int j = 0; j < t[i]; j++) r[c++] = x;
                    }
                    break;
                case "value":
                    for (int i = 1; i < t.Length; i++) {
                        x = arg[i];
                        for (int j = 0; j < t[i]; j++) r[c++] = x;
                    }
                    break;
                case "one":
                    for (int j = 0; j < r.Length; j++) r[j] = 1.0;
                    break;
                case "zero":
                    for (int j = 0; j < r.Length; j++) r[j] = 0.0;
                    break;
                default:
                    throw new Exception("Expansion Operator Syntax Error " + fn2);
            }

            r[0] = (int)arg[0] + 0.366;

            return r;
        }

        private static TimeSeries expand1(TimeSeries arg, string fn, int freq) {
            TimeSeries result = arg.tsCopy();
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] b = arg.ts[i];
                int[] t = new int[b.Length];
                int[] t0 = (int[])t.Clone();
                double a = b[0];
                int f = Duration.freq(a);
                int freq2 = freq > 12 ? 366 : freq;
                int p = Duration.period(a);
                int d = Duration.periodDate(p, f);
                int e = Duration.periodNr(d, freq2);
                int s = 0;
                for (int j = 1; j < t.Length; j++) {
                    int h = Duration.periodDate(p + j, f);
                    int g = Duration.periodNr(h, freq2);
                    t[j] = g - e;
                    t0[j] = e;
                    s += t[j];
                    e = g;
                }

                double[] r = new double[s + 1];

                int c = 1;
                double x = 0.0;
                switch (fn) {
                    case "step":
                        for (int k = 1; k < t.Length; k++) {
                            x = b[k] / t[k];
                            for (int j = 0; j < t[k]; j++) r[c++] = x;
                        }
                        break;
                    case "value":
                        for (int k = 1; k < t.Length; k++) {
                            x = b[k];
                            for (int j = 0; j < t[k]; j++) r[c++] = x;
                        }
                        break;
                    case "one":
                        for (int j = 0; j < r.Length; j++) r[j] = 1.0;
                        break;
                    case "zero":
                        for (int j = 0; j < r.Length; j++) r[j] = 0.0;
                        break;
                    default:
                        throw new Exception("Expansion Operator Syntax Error " + fn);
                }
                r[0] = (int)b[0] + 0.001 * freq2;
                if (freq != freq2) r = compressFromDaily(r, companionFn(fn), freq);
                result.ts[i] = r;
            }

            return result;
        }

        private static double[] compressFromDaily(double[] arg, string fn, int freq) {
            double a = arg[0];
            int f = Duration.freq(a);
            if (f <= freq) throw new Exception("Incorrect frequency " + f);
            int[] r = new int[arg.Length];
            r[0] = (int)arg[0];
            int[] r0 = (int[])r.Clone();

            int p = Duration.period(a);
            int d = Duration.periodDate(p, f);
            int e = Duration.periodNr(d, f);
            for (int j = 1; j < r.Length; j++) {
                int h = Duration.periodDate(e, f);
                r0[j] = e++;
                r[j] = Duration.periodNr(h, freq);
            }
            p = r[1];
            double[] r2 = new double[r[r.Length - 1] - p + 2];
            double[] r3;
            p--;

            switch (fn) {
                case "sum":
                    for (int j = 0; j < r2.Length; j++) r2[j] = 0.0; // r2[] = 0;
                    for (int j = 1; j < r.Length; j++) {
                        r2[r[j] - p] += arg[j];
                    }
                    break;
                case "max":
                    for (int j = 0; j < r2.Length; j++) r2[j] = -1.7976931348623157e308;
                    for (int j = 1; j < r.Length; j++) {
                        r2[r[j] - p] = Math.Max(arg[j], r2[r[j] - p]);
                    }
                    break;
                case "min":
                    for (int j = 0; j < r2.Length; j++) r2[j] = 1.7976931348623157e308;
                    for (int j = 1; j < r.Length; j++) {
                        r2[r[j] - p] = Math.Min(arg[j], r2[r[j] - p]);
                    }
                    break;
                case "last":
                    for (int j = 1; j < r.Length; j++) {
                        r2[r[j] - p] = arg[j];
                    }
                    break;
                case "mean":
                case "avg":
                case "amean":
                    for (int j = 0; j < r2.Length; j++) r2[j] = 0.0; // r2[] = 0.0;
                    r3 = (double[])r2.Clone();
                    for (int j = 1; j < r.Length; j++) {
                        r2[r[j] - p] += arg[j];
                        r3[r[j] - p]++;
                    }
                    for (int j = 1; j < r2.Length; j++) {
                        r2[j] /= r3[j];
                    }
                    break;
                case "gmean":
                    for (int j = 0; j < r2.Length; j++) r2[j] = 1.0; // r2[] = 1.0;
                    r3 = (double[])r2.Clone();
                    for (int j = 1; j < r.Length; j++) {
                        r2[r[j] - p] *= arg[j];
                        r3[r[j] - p]++;
                    }
                    for (int j = 1; j < r2.Length; j++) {
                        r2[j] = Math.Pow(r2[j], 1 / r3[j]);
                    }
                    break;
                case "hmean":
                    for (int j = 0; j < r2.Length; j++) r2[j] = 0.0; // r2[] = 0.0;
                    r3 = (double[])r2.Clone();
                    for (int j = 1; j < r.Length; j++) {
                        r2[r[j] - p] += 1.0 / arg[j];
                        r3[r[j] - p]++;
                    }
                    for (int j = 1; j < r2.Length; j++) {
                        r2[j] = r3[j] / r2[j];
                    }
                    break;
                default:
                    throw new Exception("Operator Argument Syntax Error " + fn);
            }
            r2[0] = r[0] + 0.001 * freq;

            int d1, d2;
            d1 = Duration.periodDate(r[1], freq);
            d2 = Duration.periodDate(r0[1], f);
            if (d1 != d2) r2[1] = Double.NaN;

            d1 = Duration.periodDateEnd(r[r.Length - 1], freq);
            d2 = Duration.periodDateEnd(r0[r0.Length - 1], f);
            if (d1 != d2) r2[r2.Length - 1] = Double.NaN;

            return r2;
        }

        private static string companionFn(string arg) {
            string fn = "";
            switch (arg) {

                // aggregation
                case "sum":
                    fn = "step";
                    break;
                case "min":
                case "max":
                    fn = "value";
                    break;

                // expansion
                case "step":
                    fn = "sum";
                    break;
                case "value":
                    fn = "max";
                    break;

                default:
                    throw new Exception("Expansion Operator Syntax Error " + fn);
            }
            return fn;
        }

    }
}
