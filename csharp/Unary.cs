
using System;

namespace Yearly {
    public class Unary {

        private static double nan = double.NaN;

        public static bool isNaN(double d) {
            return double.IsNaN(d);
        }

        public static TimeSeries negate(TimeSeries arg) {
            TimeSeries result = arg.tsCopy();
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = -a[j];
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double negate(double arg) {
            return -arg;
        }

        public static TimeSeries abs(TimeSeries arg) {
            TimeSeries result = arg.tsCopy();
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = Math.Abs(a[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double abs(double arg) {
            if (isNaN(arg)) return nan;
            return Math.Abs(arg);
        }

        public static TimeSeries trunc(TimeSeries arg) {
            TimeSeries result = arg.tsCopy();
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = ArithUtil.floor(a[j], 1e-10);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double trunc(double arg) {
            if (isNaN(arg)) return nan;
            return ArithUtil.floor(arg, 1e-10);
        }

        public static TimeSeries exp(TimeSeries arg) {
            TimeSeries result = arg.tsCopy();
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = Math.Exp(a[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double exp(double arg) {
            if (isNaN(arg)) return nan;
            return Math.Exp(arg);
        }

        public static double ln(double arg) {
            if (isNaN(arg)) return nan;
            return Math.Log(arg);
        }
        public static TimeSeries ln(TimeSeries arg) {
            TimeSeries result = arg.tsCopy();
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = Math.Log(a[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double sin(double arg) {
            if (isNaN(arg)) return nan;
            return Math.Sin(arg);
        }

        public static TimeSeries sin(TimeSeries arg) {
            TimeSeries result = arg.tsCopy();
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = Math.Sin(a[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double cos(double arg) {
            if (isNaN(arg)) return nan;
            return Math.Cos(arg);
        }

        public static TimeSeries cos(TimeSeries arg) {
            TimeSeries result = arg.tsCopy();
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = Math.Cos(a[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double tan(double arg) {
            if (isNaN(arg)) return nan;
            return Math.Tan(arg);
        }

        public static TimeSeries tan(TimeSeries arg) {
            TimeSeries result = arg.tsCopy();
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = Math.Tan(a[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

    }
}

