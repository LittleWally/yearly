using System;
using System.Diagnostics;

namespace Yearly {
    partial class Parser {
        public static string token;
        private static string[] line;
        private static int tptr;

        public static Union parse(string arg) {
            return processData(arg);
        }

        private static Union processData(string arg) {
            line = Lex.split(arg);
            if (line.Length == 1) return (null);
            tptr = 0;
            token = getNext();
            return expr(0);
        }

        private static Union binary(string arg, Union left) {
            Union[] args;
            int c;
            int bp = lbp(arg);
            switch (arg) {

                // assignment
                case ":=": return Union.assign(left, expr(bp));
                case ";": return Union.join(left, expr(bp));

                // arithmetic functions
                case "*": return Union.times(left, expr(bp));
                case "/": return Union.divide(left, expr(bp));

                case "+": return Union.plus(left, expr(bp));
                case "#+": return Union.plus(left, expr(bp), 1);
                case "+#": return Union.plus(left, expr(bp), 2);
                case "#+#": return Union.plus(left, expr(bp), 3);

                case "-": return Union.minus(left, expr(bp));
                case "#-": return Union.minus(left, expr(bp), 1);
                case "-#": return Union.minus(left, expr(bp), 2);
                case "#-#": return Union.minus(left, expr(bp), 3);

                case "#": return Union.repmv(left, expr(bp));

                case "!+": return Union.merge(left, expr(bp), 1, 1);
                case "+!": return Union.merge(left, expr(bp), 2, 1);
                case "!+!": return Union.merge(left, expr(bp), 3, 1);
                case "!-": return Union.merge(left, expr(bp), 1, -1);
                case "-!": return Union.merge(left, expr(bp), 2, -1);
                case "!-!": return Union.merge(left, expr(bp), 3, -1);

                case "**": return Union.power(left, expr(bp));
                case "||": return Union.concat(left, expr(bp));

                case "..": return Union.miota(left, expr(bp));

                case "mod": return Union.mod(left, expr(bp));
                case "smaller": return Union.smaller(left, expr(bp));
                case "larger": return Union.larger(left, expr(bp));
                case "round": return Union.round(left, expr(bp));
                case "log": return Union.log(left, expr(bp));
                case "power": return Union.power(left, expr(bp));
                case "nroot": return Union.nroot(left, expr(bp));

                case "<": return Union.lt(left, expr(bp));
                case "<=": return Union.le(left, expr(bp));
                case ">=": return Union.ge(left, expr(bp));
                case ">": return Union.gt(left, expr(bp));
                case "=": return Union.eq(left, expr(bp));
                case "<>": return Union.ne(left, expr(bp));
                case "and": return Union.and(left, expr(bp));
                case "or": return Union.or(left, expr(bp));
                case "xor": return Union.ne(left, expr(bp)); // xor is the same as ne

                case "?":
                    args = new Union[2];
                    args[0] = expr(0);
                    advance(":");
                    args[1] = expr(0);
                    return Union.conditional(left, args[0], args[1]);

                case "[":
                    args = new Union[2];
                    c = 0;
                    if (token != "]") {
                        while (true) {              // mop up all the arguments
                            args[c++] = expr(0);
                            if (token != ",") break;
                            advance(",");
                        }
                    }
                    advance("]");

                    if (c == 1) return Union.idx(left, args[0]);
                    if (c == 2) return Union.idx(left, args[0], args[1]);
                    return null;

                case "(":
                    args = new Union[42];
                    c = 0;
                    string fn = left.name;
                    if (token != ")") {
                        while (true) {
                            args[c++] = expr(0);
                            if (token != ",") break;
                            advance(",");
                        }
                    }
                    advance(")");

                    // function notation handled here for built-in functions
                    // advantage here is that these need no order of precedence assigned to them
                    // aggregation fns: // sum max min last mean avg amean gmean hmean
                    switch (fn) {
                        case "power": return Union.power(args[0], args[1]);
                        case "all": return Union.all(args[0]);
                        case "any": return Union.any(args[0]);
                        case "min": return Union.min(args[0]);
                        case "max": return Union.max(args[0]);
                        case "sum": return Union.sum(args[0]);
                        case "mean":
                        case "amean":
                        case "avg": return Union.avg(args[0]);
                        case "gmean": return Union.gmean(args[0]);
                        case "hmean": return Union.hmean(args[0]);
                        case "stdev": return Union.stdev(args[0]);
                        case "movavg": return Union.movavg(args[0], args[1]);
                        case "scalar": return Union.scsv(args[0]);
                        case "pch":
                            switch (c) {
                                case 1: return Union.pch(args[0]);
                                case 2: return Union.pch(args[0], args[1]);
                                default: return Union.pch(args[0]); // really an error
                            }
                        case "lag": return Union.lag(args[0], args[1]);

                        case "daily": return Union.daily(args[0], args[1]);
                        case "weekly": return Union.weekly(args[0], args[1]);
                        case "monthly": return Union.monthly(args[0], args[1]);
                        case "quarterly": return Union.quarterly(args[0], args[1]);
                        case "yearly":
                        case "annual": return Union.annual(args[0], args[1]);

                        case "compressYearly":
                        case "compressAnnual": return Union.compressAnnual(args[1], args[0]);
                        case "compressQuarterly": return Union.compressQuarterly(args[1], args[0]);
                        case "compressMonthly": return Union.compressMonthly(args[1], args[0]);
                        case "compressWeekly": return Union.compressWeekly(args[1], args[0]);

                        case "expandQuarterly": return Union.expandQuarterly(args[0], args[1]);
                        case "expandMonthly": return Union.expandMonthly(args[0], args[1]);
                        case "expandWeekly": return Union.expandWeekly(args[0], args[1]);
                        case "expandDaily": return Union.expandDaily(args[0], args[1]);

                        default: return null; // Union.foo(args[0], args[1]);
                    }

                // case ":=": return left.assign(expr(bp));
                default:
                    Debug.Assert(1 == 42);
                    return null;
            }
        }

        private static Union unary(string arg) {

            Union result;

            switch (arg) {
                case "+": return expr(4);
                case "-": return Union.negate(expr(4));
                case "not": return Union.not(expr(4));
                case "abs": return Union.abs(expr(4));
                case "trunc": return Union.trunc(expr(4));
                case "exp": return Union.exp(expr(4));
                case "ln": return Union.ln(expr(4));
                case "#": return Union.repmv(expr(4));
                case "get": return Union.get(expr(4));
                case "sin": return Union.sin(expr(4));
                case "cos": return Union.cos(expr(4));
                case "tan": return Union.tan(expr(4));
                case "(":
                    result = expr(0);
                    advance(")");
                    return result;
                case ")":
                    return resolve(arg);
                case "\n":
                    Debug.Assert(1 == 42);
                    return null;
                default: // handle niladic, non result returning functions here

                    /*

                    if (arg == line[0]) {
                        b = sharpPlotEnums(arg);
                        if (b) return null;
                        b = sharpPlotExtraFns(arg);
                        if (b) return null;
                        b = sharpPlotFonts(arg);
                        if (b) return null;
                        b = sharpPlotFunctions(arg);
                        if (b) return null;
                        b = sharpPlotFunctions2(arg);
                        if (b) return null;
                        b = sharpPlotPlots(arg);
                        if (b) return null;
                        b = sharpPlotProps(arg);
                        if (b) return null;
                        // b = sharpPlotSetters(arg);
                        // if (b) return null;
                        b = sharpPlotStyles(arg);
                        if (b) return null;
                        b = sharpPlotTsPlots(arg);
                        if (b) return null;
                    }

                    */

                    return resolve(arg);
            }
        }

        private static int lbp(string arg) {

            switch (arg) {
                case ")":
                    return 0;
                case "round":
                case "abs":
                case "trunc":
                case "exp":
                case "ln":
                case "log":
                case "power":
                case "nroot":
                case "mod":
                case "smaller":
                case "larger":
                    return 2;

                case "*":
                case "/":
                case "||":
                case "**":
                case "..":
                    return 6;

                case "+":
                case "-":
                case "#+":
                case "#-":
                case "+#":
                case "-#":
                case "#+#":
                case "#-#":
                case "#":
                case "!+":
                case "+!":
                case "!+!":
                case "-!":
                case "!-":
                case "!-!":
                    return 5;

                case "<":
                case "<=":
                case ">=":
                case ">":
                    return 4;

                case "=":
                case "<>":
                    return 4;

                case "and":
                    return 10;
                case "or":
                case "xor":
                    return 11;
                case "?":
                    return 12;
                case ":=":
                    return 1;
                case "(":
                case "[":
                    return 15;
                case "\n":
                    return 0;
                case ",":
                case ":":
                    return 0;
                case ";":
                    return 3;
                default:
                    // return 0;
                    throw new Exception("Syntax Error " + arg);
            }
        }

        private static Union expr(int arg) {

            string t;
            Union left;
            int rbp = arg;

            t = token;
            token = getNext();
            left = unary(t);

            if (left == null) return null;  // no result

            while (rbp < lbp(token)) {
                t = token;
                token = getNext();
                left = binary(t, left);
            }
            return left;
        }

        private static string getNext() {
            tptr++;
            return line[tptr - 1];
        }

        private static void advance(string arg) {
            if (arg != token) throw new Exception("Syntax error, expected " + arg + " got " + token);
            token = getNext();
        }

        private static Union resolve(string arg) {
            char c;
            string t;

            if (char.IsLetter(arg[0])) {
                if (token == ":=") return new Union(arg, -10);
                if (Grobals.vars.ContainsKey(arg)) {
                    Union r = Grobals.vars[arg];
                    //r.str = arg;
                    return r;
                } else {
                    if (token == ":=" || token == "(") return new Union(arg, -10);
                    else throw new Exception("Value Error");
                }
            }

            /*		if (arg.startsWith("a") && TimeSeries.validateDouble(arg.substring(1, arg.length()))) { // Hack!
                        idx = Integer.parseInt(arg.replaceFirst("a", ""));
                        return new Union(tsdata[idx]);
                    }*/
            // if (arg.indexOf(",") != 0) {
            // 	return new Union(arg);
            // }

            //		d = Double.parseDouble(arg);
            //		if (!Double.isNaN(d)) {
            //			return new Union(d);
            //		}
            //		
            if (arg.ToLower() == "nan") return new Union(double.NaN);

            // negative number landed here
            c = arg[0];
            if ((c == '-' || char.IsDigit(c)) && -1 != arg.IndexOf(' ')) {
                t = ("-2147483606.0 " + arg).Replace("  ", " ").Replace(" ", ",");
                return new Union(t);
            }
            if (c == '-' || char.IsDigit(c)) return new Union(double.Parse(arg));

            // if char, then it is a name in front of '(', i.e. power(x,2)
            if (char.IsLetter(c)) return new Union(arg, -10);

            // if it starts with ", then it's a quoted string
            t = arg.Substring(1, arg.Length - 2);
            if (c == '"') return new Union(t, -20);

            return null;
        }
    }
}
