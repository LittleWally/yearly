namespace Yearly {
    public static class StringExtensions {

        public static string Mid(this string arg, int a0, int a1) {
            return arg.Substring(a0 - 1, a1);
        }

        public static string Right(this string arg, int a0) {
            int t = arg.Length;
            return arg.Substring(t - a0, a0);
        }

        public static string Left(this string arg, int a0) {
            return arg.Substring(0, a0);
        }
    }
}
