﻿using System;

namespace Yearly {
    public class Aggr {
        // Aggregation

        private static double nan = double.NaN;

        public static bool isNaN(double d) {
            return double.IsNaN(d);
        }

        public static double scalar(TimeSeries arg) {
            double result = nan;
            if (arg.ts.Length == 0) return result;
            double[] a = arg.ts[0];
            if (a.Length <= 1) return result;
            return a[1];
        }

        public static double scalar(double arg) {
            return arg;
        }

        public static TimeSeries sum(TimeSeries arg) {
            double[] result = new double[arg.ts.Length + 1];
            result[0] = -2147483606;
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double r = 0;
                for (int j = 1; j < a.Length; j++) r += a[j];
                result[i + 1] = r;
            }
            return new TimeSeries(result);
        }

        public static double sum(double arg) {
            return arg;
        }

        public static TimeSeries avg(TimeSeries arg) {
            double[] result = new double[arg.ts.Length + 1];
            result[0] = -2147483606;
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double r = 0;
                for (int j = 1; j < a.Length; j++) r += a[j];
                result[i + 1] = r / (a.Length - 1);
            }
            return new TimeSeries(result);
        }

        public static double gmean(double arg) {
            return arg;
        }

        public static TimeSeries gmean(TimeSeries arg) {
            double[] result = new double[arg.ts.Length + 1];
            result[0] = -2147483606;
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double r = 1;
                for (int j = 1; j < a.Length; j++) r *= a[j];
                result[i + 1] = Math.Pow(r, 1.0 / (a.Length - 1));
            }
            return new TimeSeries(result);
        }

        public static double hmean(double arg) {
            return arg;
        }

        public static TimeSeries hmean(TimeSeries arg) {
            double[] result = new double[arg.ts.Length + 1];
            result[0] = -2147483606;
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double r = 0;
                for (int j = 1; j < a.Length; j++) r += 1.0 / a[j];
                result[i + 1] = (a.Length - 1) / r;
            }
            return new TimeSeries(result);
        }

        public static double avg(double arg) {
            return arg;
        }

        public static TimeSeries min(TimeSeries arg) {
            double[] result = new double[arg.ts.Length + 1];
            result[0] = -2147483606;
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double r = double.MaxValue;
                for (int j = 1; j < a.Length; j++) {
                    if (isNaN(a[j])) {
                        r = nan;
                        break;
                    }
                    r = Math.Min(a[j], r);
                }
                result[i + 1] = r;
            }
            return new TimeSeries(result);
        }

        public static double min(double arg) {
            return arg;
        }

        public static TimeSeries max(TimeSeries arg) {
            double[] result = new double[arg.ts.Length + 1];
            result[0] = -2147483606;
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double r = double.MinValue;
                for (int j = 1; j < a.Length; j++) {
                    if (isNaN(a[j])) {
                        r = nan;
                        break;
                    }
                    r = Math.Max(a[j], r);
                }
                result[i + 1] = r;
            }
            return new TimeSeries(result);
        }

        public static double max(double arg) {
            return arg;
        }

        public static TimeSeries stdev(TimeSeries arg) {
            double[] result = new double[arg.ts.Length + 1];
            result[0] = -2147483606;
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double sum1 = 0.0;
                for (int j = 1; j < a.Length; j++) sum1 += a[j];
                double sum2 = 0.0;
                for (int j = 1; j < a.Length; j++) {
                    double t = a[j] - sum1 / (a.Length - 1);
                    sum2 += t * t;
                }
                double c = 1.0 / Math.Max(1.0, a.Length - 2);
                result[i + 1] = Math.Pow(c * sum2, 0.5);
            }
            return new TimeSeries(result);
        }

        public static double stdev(double arg) {
            if (isNaN(arg)) return nan;
            return 0.0;
        }

        public static TimeSeries all(TimeSeries arg) {
            double[] result = new double[arg.ts.Length + 1];
            result[0] = -2147483606;
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double r = 1.0;
                for (int j = 1; j < a.Length; j++) {
                    if (a[j] == 0.0) {
                        r = 0.0;
                    }
                    if (isNaN(a[j])) {
                        r = nan;
                        break;
                    }
                }
                result[i + 1] = r;
            }
            return new TimeSeries(result);
        }

        public static double all(double arg) {
            return arg;
        }

        public static TimeSeries any(TimeSeries arg) {
            double[] result = new double[arg.ts.Length + 1];
            result[0] = -2147483606;
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double r = 0.0;
                for (int j = 1; j < a.Length; j++) {
                    if (a[j] != 0.0) {
                        r = 1.0;
                    }
                    if (isNaN(a[j])) {
                        r = nan;
                        break;
                    }
                }
                result[i + 1] = r;
            }
            return new TimeSeries(result);
        }

        public static double any(double arg) {
            return arg;
        }

        public static TimeSeries movavg(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = movavg(a, (int)arg);
                result.ts[i] = r;
            }
            return result;
        }

        private static double[] movavg(double[] arg, int per) {
            double[] r = new double[Math.Max(1, arg.Length - per + 1)];
            r[0] = Duration.addPer(arg[0], per);
            if (1 == r.Length) return r;
            for (int i = 1; i < r.Length; i++) {
                double t = 0;
                for (int j = i; j < i + per; j++) {
                    t = t + arg[j];
                }
                r[i] = t / per;
            }
            return r;
        }

        public static double movavg(double arg, double arg2) {
            return (arg2 == 1.0) ? arg : nan;
        }

        public static double index(TimeSeries ts, string arg) {
            if (arg.Length == 0) return nan;
            int d = (int)Duration.convDate(arg);
            if (d == -42) return nan;
            return index(ts, d);
        }

        public static double index(TimeSeries ts, double arg) {
            double result = nan;
            if (isNaN(arg)) return nan;

            for (int i = 0; i < 1; i++) {
                result = indexByDate(ts.ts[i], (int)arg);
            }
            return result;
        }

        public static double indexByDate(double[] ts, int dt) {
            double result = nan;
            if (dt < 10000) dt = dt * 100 + 1;
            if (dt < 1000000) dt = dt * 100 + 1;
            int a = Duration.periodNr(ts[0]);
            int f = Duration.freq(ts[0]);
            int b = Duration.periodNr(dt, f);
            int i = b - a;
            if (i < 0) return result;
            if (i > ts.Length - 2) return result;
            return ts[i + 1];
        }
    }
}


