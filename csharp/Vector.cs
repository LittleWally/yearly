namespace Yearly {
    class Vector {

        public static double[] cat(double a, double[] b) {
            double[] r = new double[b.Length + 1];
            r[0] = a;
            for (int i = 0; i < b.Length; i++) r[i + 1] = b[i];
            return r;
        }

        public static double[] cat(double[] a, double b) {
            double[] r = new double[a.Length + 1];
            r[a.Length] = b;
            for (int i = 0; i < a.Length; i++) r[i] = a[i];
            return r;
        }

        public static double[] cat(double[] a, double[] b) {
            double[] r = new double[a.Length + b.Length];
            int c = 0;
            for (int i = 0; i < a.Length; i++) r[c++] = a[i];
            for (int i = 0; i < b.Length; i++) r[c++] = b[i];
            return r;
        }

        public static double[] cat(double a, double b) {
            return new double[] { a, b };
        }

        public static TimeSeries concat(double arg, double arg2) {
            double[] t = new double[] { -2147483606.0, arg, arg2 };
            return new TimeSeries(t);
        }

        public static TimeSeries miota(double a, double b) {
            double[] r = new double[((int)b - (int)a) + 1];
            for (int i = 0; i < r.Length; i++) r[i] = i + a;
            return new TimeSeries(r);
        }
    }
}

