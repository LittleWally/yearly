using System;
using System.Collections.Generic;

// using Word = Microsoft.Office.Interop.Word;

namespace Yearly {
    public class Grobals {
        public static double fuzz;
        public static int decimals;
        public static bool csv;
        public static bool quit;
        public static string dbPath;
        public static Dictionary<string, Union> vars;
        public static Dictionary<string, string[]> fns;
        // public static SharpPlot sp;
        // public static Word.Selection sel;
        // public static Word.Cells cels;
        // public static Word.Cell cel;
        // public static Word.Document doc;
        // public static Word.Table tab;
        public static char sep;
        public static char sep2;
        public static double adj;
        public static string tempDir;
        public static string filename;

        /*
        public static void init() {
            if (Gdi.osInfo() >= 602) { // not durable
                adj = 1.5;
            } else {
                adj = 1.25;
            }
            // tempDir = "C:\\Users\\" + Environment.GetEnvironmentVariable("USERNAME") + "\\AppData\\Roaming\\Microsoft\\Word\\";
            tempDir = tempDirName();
        }
        */

        public static void initRepl() {
            fuzz = 1e-10;
            decimals = 10;
            csv = false;
            dbPath = "";
            sep = ',';
            sep2 = ';';
            quit = false;

            /*
            if (Gdi.osInfo() > 602) { // not durable
                adj = 1.5;
            } else {
                adj = 1.25;
            }
            */

            vars = new Dictionary<string, Union>(); // (100, (float)0.8);
            tempDir = tempDirName();
        }

        public static void resetAddin() {
            fuzz = 1e-10;
            decimals = 10;
            csv = false;
            dbPath = "";
            sep = ',';
            sep2 = ';';

            /*
            if (Gdi.osInfo() > 602) { // not durable
                adj = 1.5;
            } else {
                adj = 1.25;
            }

            */

            vars = new Dictionary<string, Union>(); // (100, (float)0.8);
            fns = new Dictionary<string, string[]>();
            tempDir = tempDirName();
        }

        private static string tempDirName() {
            return "C:\\Users\\" + Environment.GetEnvironmentVariable("USERNAME") + "\\AppData\\Local\\Temp\\";
        }

        public bool checkVarName(string arg) {
            return !(vars.ContainsKey(arg));
        }

        public static IDictionary<string, int> BarChartStylesArray;
        public static IDictionary<string, int> BoxPlotStylesArray;
        public static IDictionary<string, int> BubbleChartStylesArray;
        public static IDictionary<string, int> CloudChartStylesArray;
        public static IDictionary<string, int> CaptionStylesArray;
        public static IDictionary<string, int> ContourPlotStylesArray;
        public static IDictionary<string, int> ColourArray; // from system.drawing
        public static IDictionary<string, int> DataStylesArray;
        public static IDictionary<string, int> DatumTagStylesArray;
        public static IDictionary<string, int> DialChartStylesArray;
        public static IDictionary<string, int> EquationStylesArray;
        public static IDictionary<string, int> FillStylesArray;
        public static IDictionary<string, int> FootnoteStylesArray;
        public static IDictionary<string, int> FrameStylesArray;
        public static IDictionary<string, int> GanttChartStylesArray;
        public static IDictionary<string, int> HeadingStylesArray;
        public static IDictionary<string, int> HistogramStylesArray;
        public static IDictionary<string, int> IAxisStylesArray;
        public static IDictionary<string, int> ITickStylesArray;
        public static IDictionary<string, int> KeyStylesArray;
        public static IDictionary<string, int> LabelStylesArray;
        public static IDictionary<string, int> LineGraphStylesArray;
        public static IDictionary<string, int> LineStylesArray;
        public static IDictionary<string, int> MinMaxChartStylesArray;
        public static IDictionary<string, int> NetworkMapStylesArray;
        public static IDictionary<string, int> NoteStylesArray;
        public static IDictionary<string, int> ParityStylesArray;
        public static IDictionary<string, int> PieChartStylesArray;
        public static IDictionary<string, int> PolarChartStylesArray;
        public static IDictionary<string, int> ResponsePlotStylesArray;
        public static IDictionary<string, int> ScalebarStylesArray;
        public static IDictionary<string, int> ScatterPlotStylesArray;
        public static IDictionary<string, int> StepChartStylesArray;
        public static IDictionary<string, int> TableStylesArray;
        public static IDictionary<string, int> TowerChartStylesArray;
        public static IDictionary<string, int> TraceChartStylesArray;
        public static IDictionary<string, int> TreeMapStylesArray;
        public static IDictionary<string, int> TrellisStylesArray;
        public static IDictionary<string, int> TriangleStylesArray;
        public static IDictionary<string, int> ValueTagStylesArray;
        public static IDictionary<string, int> VectorStylesArray;
        public static IDictionary<string, int> VennDiagramStylesArray;
        public static IDictionary<string, int> XAxisStylesArray;
        public static IDictionary<string, int> XBarChartStylesArray;
        public static IDictionary<string, int> XTickStylesArray;
        public static IDictionary<string, int> YAxisStylesArray;
        public static IDictionary<string, int> YTickStylesArray;
        public static IDictionary<string, int> ZAxisStylesArray;
        public static IDictionary<string, int> ZTickStylesArray;
    }
}
