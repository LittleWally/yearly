

public interface VTLCalculationManager {

    /**
	 * Executes the given expression against the supplied data, returning the result
	 * @param expression
	 * @param data
	 * @return
	 */
    string[] execute(string expression, string[][] timeSeries);

    void validate(string expression);

}

