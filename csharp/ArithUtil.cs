﻿using System;

namespace Yearly {
    class ArithUtil {

        public static double log10(double x) {
            double bias = 5E-16;
            double t = Math.Log(x);
            return t / Math.Log(10) + Math.Sign(t) * bias;
        }

        public static double log(double x, double y) {
            double bias = 5E-16;
            double t = Math.Log(x);
            return t / Math.Log(y) + Math.Sign(t) * bias;
        }

        public static double fmod(double x, double y) {
            return x - y * floor(x / y, 0.0000000001);
        }

        public static bool eq(double a, double b, double c) {
            double t = Math.Abs(a - b);
            a = Math.Abs(a);
            b = Math.Abs(b);
            double u = Math.Max(a, b);
            if (c == 0) c = 0.0000000001; // 1E-10
                                          // if (a > b) { u = a;} else {u = b;}
            return t <= c * u;
        }

        public static int floor(double a, double c) {
            double b = Math.Abs(a);
            int r = (int)(Math.Sign(a) * Math.Floor(0.5 + b));         // nearest integer to a.
            if (b < 1) { b = 1; }
            return ((r - a) > c * b) ? r + 1 : r;
        }

        public static int ceiling(double a, double c) {
            return -floor(-a, c);
        }

        public static bool ne(double a, double b, double c) {
            return !eq(a, b, c);
        }

        public static bool lt(double a, double b, double c) {
            return (a < b) && ne(a, b, c);
        }

        public static bool le(double a, double b, double c) {
            return (a <= b) || eq(a, b, c);
        }

        public static bool ge(double a, double b, double c) {
            return (a >= b) || eq(a, b, c);
        }
        /**
         * a < b with default comparison tolerance (1e-10)
         * @param a left argument to <
         * @param b right argument to <
         * @return true or false
         */
        public static bool gt(double a, double b) {
            return (a > b) && ne(a, b, 1e-10);
        }
        /**
         * a < b with non-default comparison tolerance
         * @param a left argument to <
         * @param b right argument to <
         * @param c comparison tolerance (i.e. 1e-16)
         * @return true or false
         */

        public static bool gt(double a, double b, double c) {
            return (a > b) && ne(a, b, c);
        }

        public static bool xor(double a, double b, double c) {
            return !eq(a, b, c);
        }

        public static bool and(double a, double b, double c) {
            return eq(a, 1.0, c) && eq(b, 1.0, c);
        }

        public static bool or(double a, double b, double c) {
            return eq(a, 1.0, c) || eq(b, 1.0, c);
        }

        public static bool not(double a, double c) {
            return !eq(a, 1.0, c);
        }

        public static double rnd(double a, double b) {
            double t = Math.Pow(10, b);
            return floor(a * t, 1e-10) / t;
        }

    }

}

