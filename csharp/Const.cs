﻿using System;

namespace Yearly {

    public class CONST {
        public static double DOUBLE_MERSENNE_PRIME = -2147483647;
        public static double NOT_A_NUMBER = Double.NaN;
    }
}
