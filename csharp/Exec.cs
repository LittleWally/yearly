using System;

namespace Yearly {
    public class Exec {
        public static string EXIT_COMMAND = "#exit";

        public static string exec(string arg) {
            string result = "";

            //		GeneralMatrix mat = new GeneralMatrix(new double[][]{{5,8,3},{4,7,2},{1,6,9}},3,3);
            //		mat = mat.inverse();
            //		mat = new GeneralMatrix(new double[][]{{4,7,2},{9,1,8},{3,5,6},{4,7,2}},4,3);
            //		mat = mat.inverse();

            string input = arg.Trim();

            if (input.Length == EXIT_COMMAND.Length && input.ToLower() == EXIT_COMMAND) {
                result = "Exiting Yearly";
                Grobals.quit = true;
                return result;
            }
            if (input.Length != 0) {
                if ((char)input[0] == '#') {
                    result = sysCommand(input);
                } else {

                    result = execute(input);
                    /*
                    try {
                        result = execute(input);
                    }
                    catch (Exception e) {
                        MessageBox.Show(input, "Error", MessageBoxButtons.OK);
                        result = "Error (Syntax) in expression";
                    }
                    */
                }
            }
            return result;
        }

        private static string sysCommand(string arg) {
            string[] t = arg.Split(' ');
            string result = "";

            switch (t[0]) {
                case "#dbpath":
                    if (t.Length == 1) {
                        result = "is " + Grobals.dbPath;
                        return result;
                    }
                    Grobals.dbPath = t[1];
                    return result;

                case "#csv":
                    if (t.Length == 1) {
                        result = "is " + onoff(Grobals.csv);
                        return result;
                    }
                    if (t[1] == "on") {
                        Grobals.csv = true;
                        return result;
                    }
                    if (t[1] == "off") {
                        Grobals.csv = false;
                        return result;
                    }
                    break;

                case "#help":
                    if (t.Length == 1) {
                        result = "";
                        result += "Binary arithmetic functions: ";
                        result += "+ - * / mod min max round log power nroot ";
                        result += "Binary logical functions: ";
                        result += "< <= >= > = <> and or xor ";
                        result += "Unary arithmetic functions: ";
                        result += "+ - abs trunc exp ln ";
                        result += "Unary logical functions :";
                        result += "not ";
                        return result;
                    }
                    break;

                default:
                    result = "Incorrect command " + arg;
                    break;
            }
            return result;
        }

        private static string onoff(bool arg) {
            return arg ? "on" : "off";
        }

        public static string execute(string expression) {
            Union t = null;
            t = Parser.parse(expression);
            /*
            try {
                t = Parser.parse(expression);
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                MessageBox.Show(expression, "Error", MessageBoxButtons.OK);
                return e.Message;
            }
            */
            if (t == null) return null;
            if (t.contents == 1) return t.getScalar() + "";
            if (t.contents == -20) return t.str;
            return t.getSeries().toString();
        }
    }
}

