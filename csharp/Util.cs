using System;
using System.Collections.Generic;
using System.Text;

namespace Yearly {
    public class Util {

        public static double signum(double arg) {
            if (arg == 0.0) return 0.0;
            if (arg > 0.0) return 1.0;
            return -1.0;
        }
    }
}
