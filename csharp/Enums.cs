﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yearly {
    public enum Aggregation {
        Sum = 1,
        Max = 2,
        Min = 3,
        First = 4,
        LastT = 5,
        Firstdate = 6,
        Lastdate = 7,
        Peak = 8,
        Trough= 9,
        Variance = 10,
        Mean = 11,
        Median = 12,
        Stdev = 13,
        Mode = 14,
        Geomean = 15,
        Countmv = 16,
        Countval = 17,
        Percentile = 18,
    }

    public enum Frequency {
        Annual = 1,
        Semiannual = 2,
        Triannual = 3,
        Quarterly = 4,
        Bimonthly = 6,
        Monthly = 12,
        Twicemonthly = 24,
        Biweekly = 26,
        Tenday = 36,
        Weekly = 53,
        Business = 262,
        Daily = 366,
    }
}
