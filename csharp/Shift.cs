using System;

namespace Yearly {
    public class Shift {
        // Shift

        private static double nan = double.NaN;

        public static bool isNaN(double d) {
            return double.IsNaN(d);
        }

        public static TimeSeries pch(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = pch(a, (int)arg);
                result.ts[i] = r;
            }
            return result;
        }

        public static double[] pch(double[] arg, int per) {
            int p = (int)per;
            double[] r = new double[Math.Max(1, arg.Length - p)];
            r[0] = Duration.addPer(arg[0], p);
            if (1 == r.Length) return r;
            for (int i = 1; i < r.Length; i++) {
                r[i] = 100 * (arg[i + p] / arg[i] - 1);
            }
            return r;
        }

        public static double pch(double arg, double arg2) {
            return nan;
        }

        public static TimeSeries lag(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = (double[])ts.ts[i].Clone();
                a[0] = Duration.addPer(a[0], (int)arg);
                result.ts[i] = a; // unnecessary
            }
            return result;
        }

        public static double lag(double arg, double arg2) {
            return nan;
        }
    }
}
