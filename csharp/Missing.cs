namespace Yearly {
    public class Missing {

        public static TimeSeries repmv(double s, TimeSeries arg) {
            TimeSeries result = arg.tsCopy();
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] b = arg.ts[i];
                double[] r = new double[b.Length];
                r[0] = b[0];
                for (int j = 1; j < b.Length; j++) {
                    r[j] = double.IsNaN(b[j]) ? s : b[j];
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double repmv(double b, double a) {
            return double.IsNaN(a) ? b : a;
        }
    }
}

