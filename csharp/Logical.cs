using System;

namespace Yearly {
    public class Logical {
        private static double nan = double.NaN;

        public static bool isNaN(double d) {
            return double.IsNaN(d);
        }

        public static TimeSeries eq(TimeSeries ts, TimeSeries arg) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    r[j] = isNaN(a2[j]) || isNaN(b2[j]) ? nan : ArithUtil.eq(a2[j], b2[j], 1e-10) ? 1.0 : 0.0;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries eq(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = a[j] == nan ? nan : ArithUtil.eq(a[j], arg, 1e-10) ? 1.0 : 0.0;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries eq(double arg, TimeSeries ts) {
            return eq(ts, arg);
        }

        public static double eq(double arg1, double arg2) {
            if (isNaN(arg1)) return nan;
            if (isNaN(arg2)) return nan;
            return ArithUtil.eq(arg1, arg2, 1e-10) ? 1.0 : 0.0;
        }

        public static TimeSeries ne(TimeSeries ts, TimeSeries arg) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    r[j] = isNaN(a2[j]) || isNaN(b2[j]) ? nan : ArithUtil.ne(a2[j], b2[j], 1e-10) ? 1.0 : 0.0;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries ne(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = a[j] == nan ? nan : ArithUtil.ne(a[j], arg, 1e-10) ? 1.0 : 0.0;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries ne(double arg, TimeSeries ts) {
            return ne(ts, arg);
        }

        public static double ne(double arg1, double arg2) {
            if (isNaN(arg1)) return nan;
            if (isNaN(arg2)) return nan;
            return ArithUtil.ne(arg1, arg2, 1e-10) ? 1.0 : 0.0;
        }

        public static TimeSeries lt(TimeSeries ts, TimeSeries arg) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    r[j] = isNaN(a2[j]) || isNaN(b2[j]) ? nan : ArithUtil.lt(a2[j], b2[j], 1e-10) ? 1.0 : 0.0;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries lt(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = a[j] == nan ? nan : ArithUtil.lt(a[j], arg, 1e-10) ? 1.0 : 0.0;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries lt(double arg, TimeSeries ts) {
            return gt(ts, arg);
        }

        public static double lt(double arg1, double arg2) {
            if (isNaN(arg1)) return nan;
            if (isNaN(arg2)) return nan;
            return ArithUtil.lt(arg1, arg2, 1e-10) ? 1.0 : 0.0;
        }

        public static TimeSeries le(TimeSeries ts, TimeSeries arg) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    r[j] = isNaN(a2[j]) || isNaN(b2[j]) ? nan : ArithUtil.le(a2[j], b2[j], 1e-10) ? 1.0 : 0.0;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries le(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = a[j] == nan ? nan : ArithUtil.le(a[j], arg, 1e-10) ? 1.0 : 0.0;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries le(double arg, TimeSeries ts) {
            return ge(ts, arg);
        }

        public static double le(double arg1, double arg2) {
            if (isNaN(arg1)) return nan;
            if (isNaN(arg2)) return nan;
            return ArithUtil.le(arg1, arg2, 1e-10) ? 1.0 : 0.0;
        }

        public static TimeSeries gt(TimeSeries ts, TimeSeries arg) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    r[j] = isNaN(a2[j]) || isNaN(b2[j]) ? nan : ArithUtil.gt(a2[j], b2[j], 1e-10) ? 1.0 : 0.0;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries gt(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = a[j] == nan ? nan : ArithUtil.gt(a[j], arg, 1e-10) ? 1.0 : 0.0;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries gt(double arg, TimeSeries ts) {
            return lt(ts, arg);
        }

        public static double gt(double arg1, double arg2) {
            if (isNaN(arg1)) return nan;
            if (isNaN(arg2)) return nan;
            return ArithUtil.gt(arg1, arg2, 1e-10) ? 1.0 : 0.0;
        }

        public static TimeSeries ge(TimeSeries ts, TimeSeries arg) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    r[j] = isNaN(a2[j]) || isNaN(b2[j]) ? nan : ArithUtil.le(a2[j], b2[j], 1e-10) ? 1.0 : 0.0;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries ge(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = a[j] == nan ? nan : ArithUtil.ge(a[j], arg, 1e-10) ? 1.0 : 0.0;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries ge(double arg, TimeSeries ts) {
            return le(ts, arg);
        }

        public static double ge(double arg1, double arg2) {
            if (isNaN(arg1)) return nan;
            if (isNaN(arg2)) return nan;
            return ArithUtil.ge(arg1, arg2, 1e-10) ? 1.0 : 0.0;
        }

        public static TimeSeries and(TimeSeries ts, TimeSeries arg) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    r[j] = isNaN(a2[j]) || isNaN(b2[j]) ? nan : a2[j] == 1.0 && b2[j] == 1.0 ? 1.0 : 0.0;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries and(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = a[j] == nan ? nan : a[j] == 1.0 && arg == 1.0 ? 1.0 : 0.0;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries and(double arg, TimeSeries ts) {
            return and(ts, arg);
        }

        public static double and(double arg1, double arg2) {
            if (isNaN(arg1)) return nan;
            if (isNaN(arg2)) return nan;
            return arg1 == 1.0 && arg2 == 1.0 ? 1.0 : 0.0;
        }

        public static TimeSeries or(TimeSeries ts, TimeSeries arg) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    r[j] = isNaN(a2[j]) || isNaN(b2[j]) ? nan : a2[j] == 1.0 || b2[j] == 1.0 ? 1.0 : 0.0;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries or(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = a[j] == nan ? nan : a[j] == 1.0 || arg == 1.0 ? 1.0 : 0.0;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries or(double arg, TimeSeries ts) {
            return or(ts, arg);
        }

        public static double or(double arg1, double arg2) {
            if (isNaN(arg1)) return nan;
            if (isNaN(arg2)) return nan;
            return arg1 == 1.0 || arg2 == 1.0 ? 1.0 : 0.0;
        }

        public static TimeSeries not(TimeSeries arg) {
            TimeSeries result = arg.tsCopy();
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] a = arg.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = a[j] == nan ? nan : a[j] == 1.0 ? 0.0 : 1.0;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double not(double arg) {
            if (isNaN(arg)) return nan;
            return arg == 1.0 ? 0.0 : 1.0;
        }
    }
}

