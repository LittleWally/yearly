﻿using System;
using Microsoft.VisualBasic;

namespace Yearly {
    public class Duration {
        private static double nan = double.NaN;

        public static int periodNr(double x) {
            int y = (int)x;
            int f = freq(x);
            return periodNr(y, f);
        }

        public static int periodNr(int y, int f) {
            switch (f) {
                case 1: return yearlyNr(y);
                case 2: return semiAnnualNr(y);
                case 3: return triAnnualNr(y);
                case 4: return quarterlyNr(y);
                case 6: return biMonthlyNr(y);
                case 12: return monthlyNr(y);
                case 24: return twiceMonthlyNr(y);
                case 26: return biWeeklyNr(y);
                case 36: return tendayNr(y);
                case 53: return weeklyNr(y);
                case 262: return businessNr(y);
                case 366: return dailyNr(y);
                default: return -1;
            }
        }

        public static int freq(double x) {
            double t;
            int r;
            t = ((x + 0.00000001) - (int)x);
            r = (int)(t * 1000);
            switch (r) {
                case 100:
                case 10:
                case 1: return 1;

                case 200:
                case 20:
                case 2: return 2;

                case 300:
                case 30:
                case 3: return 3;

                case 400:
                case 40:
                case 4: return 4;

                case 600:
                case 60:
                case 6: return 6;

                case 120:
                case 12: return 12;

                case 240:
                case 24: return 24;

                case 260:
                case 26: return 26;

                case 360:
                case 36: return 36;

                case 530:
                case 53: return 53;

                case 262: return 262;

                case 366: return 366;

                default: return -1; // assert false
            }
        }

        public static int period(double x) {
            int f = freq(x);
            int d = (int)x;
            switch (f) {
                case 1: return yearlyNr(d);
                case 2: return semiAnnualNr(d);
                case 3: return triAnnualNr(d);
                case 4: return quarterlyNr(d);
                case 6: return biMonthlyNr(d);
                case 12: return monthlyNr(d);
                case 24: return twiceMonthlyNr(d);
                case 26: return biWeeklyNr(d);
                case 36: return tendayNr(d);
                case 53: return weeklyNr(d);
                case 262: return businessNr(d);
                case 366: return dailyNr(d);
                default: return -1;
            }
        }

        public static int periodDate(int x, int f) {
            switch (f) {
                case 1: return yearlyDate(x);
                case 2: return semiAnnualDate(x);
                case 3: return triAnnualDate(x);
                case 4: return quarterlyDate(x);
                case 6: return biMonthlyDate(x);
                case 12: return monthlyDate(x);
                case 24: return twiceMonthlyNr(x);
                case 26: return biWeeklyNr(x);
                case 36: return tendayNr(x);
                case 53: return weeklyDate(x);
                case 262: return businessNr(x);
                case 366: return dateRep(x);
                default: return -1;
            }
        }

        public static double tsDate(int x, int f) {
            return periodDate(x, f) + f / 1000.0;
        }

        public static int periodDateEnd(int x, int f) {
            switch (f) {
                case 1: return yearlyDateEnd(x);
                case 2: return semiAnnualDateEnd(x);
                case 3: return triAnnualDateEnd(x);
                case 4: return quarterlyDateEnd(x);
                case 6: return biMonthlyDateEnd(x);
                case 12: return monthlyDateEnd(x);
                case 24: return twiceMonthlyNr(x);
                case 26: return biWeeklyNr(x);
                case 36: return tendayNr(x);
                case 53: return weeklyDateEnd(x);
                case 262: return businessNr(x);
                case 366: return dateRep(x);
                default: return -1;
            }
        }

        public static int firstWeek(int x) {
            return weeklyNr(dateRep(week1(x)));
        }

        public static double addPer(double x, int p) {
            int f = freq(x);
            int t = periodDate(p + period(x), f);
            double r = t + f / 1000.0;
            return r;
        }

        public static double convDate(string arg) {
            string[] t;
            int r;
            double z;

            if (arg.Length == 4) {
                int dno = dayNumber(1231 + 10000 * int.Parse(arg));
                return dateRep(dno) + 0.001;
            }

            if (arg.Contains("S")) {
                t = arg.Replace("-", "").Split('S');
                r = 101 + 600 * (int.Parse(t[1]) - 1);
                r = r + 10000 * int.Parse(t[0]);
                int dno = dayNumber(semiAnnualDateEnd(semiAnnualNr(r)));
                return dateRep(dno) + 0.002;
            }

            if (arg.Contains("T")) {
                t = arg.Replace("-", "").Split('T');
                r = 101 + 400 * (int.Parse(t[1]) - 1);
                r = r + 10000 * int.Parse(t[0]);
                int dno = dayNumber(triAnnualDateEnd(triAnnualNr(r)));
                return dateRep(dno) + 0.003;
            }

            if (arg.Contains("Q")) {
                // t = arg.Replace("Q", "").Split('Q');
                t = arg.Replace("-", "").Split('Q');
                r = 101 + 300 * (int.Parse(t[1]) - 1);
                r = r + 10000 * int.Parse(t[0]);
                int dno = dayNumber(quarterlyDateEnd(quarterlyNr(r)));
                return dateRep(dno) + 0.004;
            }

            if (arg.Contains("W")) {
                t = arg.Replace("-", "").Split('W');
                //			r = 101 + 300 * (VbaUtil.clng(t[1]) - 1);
                //			r = r + 10000 * VbaUtil.clng(t[0]);
                //			int dayNumber =  dayNumber(weeklyDateEnd(DurationUtil.weeklyNr(r)));
                int y = int.Parse(t[0]);
                int w = int.Parse(t[1]);
                int d = week(y, w);
                z = dateRep(d) + 0.053;
                return z;
            }

            t = arg.Split('-');

            if (2 == t.Length) {
                r = 1 + 100 * int.Parse(t[1]);
                r = r + 10000 * int.Parse(t[0]);
                int dno = dayNumber(monthlyDateEnd(monthlyNr(r)));
                return dateRep(dno) + 0.012;
            }

            if (3 == t.Length) {
                int dno = dayNumber(int.Parse(arg.Replace("-", "")));
                return dateRep(dno) + 0.366;
            }

            return -42;
        }

        public static string reformatDate(double arg) {
            int date = (int)Math.Floor(arg);
            double t = (arg - date) * 1000;
            int freq = (int)(t + 0.00001);
            int y = date / 10000;
            int m = (date % 10000) / 100;
            int d = date % 100;

            switch (freq) {
                case 1:
                case 100: return "" + y;

                case 2:
                case 200: return y + "-S" + ((m - 1) / 6 + 1);

                case 3:
                case 300: return y + "-T" + ((m - 1) / 4 + 1);

                case 4:
                case 400: return y + "-Q" + ((m - 1) / 3 + 1);

                case 53:
                case 530:
                    int w1 = week(y, 1);
                    int w2 = dayNumber(date);
                    int w3 = (w2 - w1) / 7 + 1;
                    return y + "-W" + ("0" + w3).Right(2);

                case 12:
                case 120: return y + "-" + ("0" + m).Right(2);

                case 366: return y + "-" + ("0" + m).Right(2) + "-" + ("0" + d).Right(2);

                default: return null;
            }
        }

        public static string nextFormattedDate(string arg) {
            double d = convDate(arg);
            int c = periodNr(d);
            int f = c + 1;
            double g = periodDate(f, freq(d));
            double e = d - Math.Floor(d);
            return reformatDate(e + g);
        }

        /**
         * dateRep converts an Excel day number (in reality, a Julian astronomical
         * date offset by 693900 to be consistent with Excel after 29 Feb 1900) 
         * and returns an int date in ISO format.
         * @param x int Excel day number
         * @return int ISO format date (i.e. 20011225)
         */

        public static int dateRep(int x) {
            int z = 100 * (x + 693900);
            int c = (z - 25) / 3652425;
            int t = 100 * ((z - c * 3652425 - 25) / 100);
            int y = (t + 75) / 36525;
            int d = 100 * ((175 + t - 36525 * y) / 100);
            int m = (d - 60) / 3060;
            int r1 = (50 + d - m * 3060) / 100;
            int r2 = 100 * (1 + (m + 2) % 12);
            int r3 = 10000 * (c * 100 + y);
            if (m >= 10) r3 = r3 + 10000;
            return r1 + r2 + r3;
        }

        /**
         * dayNumber converts a ISO integer date to Julian astronomical date
         * offset by 693900 to be consistent with Excel dates starting after
         * 29 February 1900.  Dates before 29 Feb 1900 will be inconsistent with
         * Excel.  (See "Leap year bug" in Wikipedia)  Prior to adding the Excel 
         * date constant, day 1 of this internal calendar is 1 March 0. (1 BC)
         * @param x int ISO date, i.e. 20011225
         * @return int day number
         */

        public static int dayNumber(int x) {
            int m = x % 10000 / 100;
            int d = x % 100;
            int y = x / 10000;
            if (m <= 2) y = y - 1;
            int t = (40 + d * 100 + 3060 * ((m + 9) % 12)) / 100;
            int y1 = y / 100 * 3652425 / 100;
            int y2 = y % 100 * 36525 / 100;
            return y1 + y2 + t - 693900;
        }

        public static int dayBefore(int x) {
            return dateRep(dayNumber(x) - 1);
        }

        /**
         * Check if an int is a valid ISO int date by doing a quick
         * round trip through dayNumber and dateRep.
         * dayNumber tolerates badly formed dates and returns an answer,
         * which when re=converted to ISO, will be a correctly formed date
         * different than the value we started with.  The values are not
         * relevant, only that they are different.
         * @param ISO int date
         * @return true or false
         */

        public static bool isDate(int x) {
            return x == dateRep(dayNumber(x));
        }

        public static double avg(double[] vec, int span, int start) {
            double r = 0.0;
            for (int j = 0; j < span; j++) {
                if (double.IsNaN(vec[start + j])) return nan;
                r += vec[start + j];
            }
            return r / span;
        }

        private static int week1(int x) {
            int b;
            b = dayNumber(x * 10000 + 101);
            return b + 3 - (b % 7 + 1) % 7;
        }

        private static int yearlyNr(int x) {
            return x / 10000;
        }

        private static int semiAnnualNr(int x) {
            int m = x / 100 % 100;
            int t = x / 10000 * 2;
            if (m > 6) t++;
            return t;
        }

        private static int triAnnualNr(int x) {
            int m = x / 100 % 100;
            int t = x / 10000 * 3;
            if (m > 4) t++;
            if (m > 8) t++;
            return t;
        }

        private static int quarterlyNr(int x) {
            int m = x / 100 % 100;
            int t = x / 10000 * 4;
            if (m > 3) t++;
            if (m > 6) t++;
            if (m > 9) t++;
            return t;
        }

        private static int biMonthlyNr(int x) {
            int m = x / 100 % 100;
            int t;
            t = x / 10000 * 6;
            if (m > 2) t++;
            if (m > 4) t++;
            if (m > 6) t++;
            if (m > 8) t++;
            if (m > 10) t++;
            return t;
        }

        private static int monthlyNr(int x) {
            return 12 * (x / 10000) + x / 100 % 100 - 1;
        }

        private static int twiceMonthlyNr(int x) {
            int d;
            int t;
            d = x % 100;
            t = 24 * (x / 10000) + 2 * (x / 100 % 100 - 1);
            if (d > 15) t++;
            return t;
        }

        private static int tendayNr(int x) {
            int d;
            int t;
            d = x / 1 % 100;
            t = 36 * (x / 10000) + 3 * (x / 100 % 100 - 1);
            if (d > 10) t++;
            if (d > 20) t++;
            return t;
        }

        private static int weeklyNr(int x) {
            return (dayNumber(x) - 2) / 7;
        }

        private static int biWeeklyNr(int x) {
            return (dayNumber(x) - 2) / 14;
        }

        private static int businessNr(int x) {
            return dayNumber(x) * 5 / 7;  // not final
        }

        private static int dailyNr(int x) {
            return dayNumber(x);
        }

        private static int monthlyDate(int x) {
            return 1 + (x % 12 + 1) * 100 + x / 12 * 10000;
        }

        private static int weeklyDate(int x) {
            return dateRep(2 + x * 7);
        }

        private static int quarterlyDate(int x) {
            return 1 + (x % 4 * 3 + 1) * 100 + x / 4 * 10000;
        }

        private static int quarterlyDateEnd(int x) {
            int y;
            int t;
            y = x + 1;
            t = 1 + (y % 4 * 3 + 1) * 100 + y / 4 * 10000;
            return dayBefore(t);
        }

        private static int biMonthlyDate(int x) {
            return 1 + (x % 6 * 2 + 1) * 100 + x / 6 * 10000;
        }

        private static int biMonthlyDateEnd(int x) {
            int t = 1 + (x % 6 * 2 + 1) * 100 + x / 6 * 10000;
            return dayBefore(t);
        }

        private static int semiAnnualDate(int x) {
            int t = 1 + (x % 2 * 6 + 1) * 100 + x / 2 * 10000;
            return t;
        }

        private static int semiAnnualDateEnd(int x) {
            int y = x + 1;
            int t = 1 + (y % 2 * 6 + 1) * 100 + y / 2 * 10000;
            return dayBefore(t);
        }

        private static int triAnnualDate(int x) {
            int t = 1 + (x % 3 * 4 + 1) * 100 + x / 3 * 10000;
            return t;
        }

        private static int triAnnualDateEnd(int x) {
            int y = x + 1;
            int t = 1 + (y % 3 * 4 + 1) * 100 + y / 3 * 10000;
            return dayBefore(t);
        }

        private static int yearlyDate(int x) {
            return 101 + x * 10000;
        }

        private static int monthlyDateEnd(int x) {
            int y;
            int t;
            y = x + 1;
            t = 1 + (y % 12 + 1) * 100 + y / 12 * 10000;
            return dayBefore(t);
        }

        private static int weeklyDateEnd(int x) {
            return dateRep(8 + x * 7); //((x + 1) * 7 - 1);
        }

        // private static int biMonthlyDateEnd(int x) {
        // int y;
        // int t;
        // y = x + 1;
        // t = 1 + (y % 2 * 6 + 1) * 100 + y / 2 * 10000;
        // return Ut.dayBefore(t);
        // }

        private static int yearlyDateEnd(int x) {
            int y;
            int t;
            y = x + 1;
            t = 101 + y * 10000;
            return dayBefore(t);
        }

        /**
         * Return int ISO week starting date (a Monday) 
         * for a year and week of a weekly date
         * @param year - i.e. 2001 in 2001-W01
         * @param wk - i.e. 1 in 2001-W01
         * @return int ISO date of first date in year
         */

        public static int week(int year, int wk) {
            int[] wb = { 0, -1, -2, -3, 3, 2, 1 };
            int d = dayNumber(101 + year * 10000);
            int t = (d - 2) % 7;
            d += wb[t];
            d += 7 * (wk - 1);
            return d;
        }

        /*
         * Public Function reduce(a() As Double, f As Integer, an As Integer) As
         * Double() Dim d() As Long Dim e() As Long Dim b As Long Dim i As Long, j As
         * Long, k As Long, l As Long Dim f0 As Integer Dim z As Long Dim r() As Double
         * ReDim d(UBound(a)) ReDim e(UBound(a)) b = periodNr(a(0)) f0 = freq(a(0)) For
         * i = 1 To UBound(d) d(i) = period(periodDate(b + i - 1, f0), f) Next i ReDim
         * r(UBound(d)) z = d(1) j = 1 k = 1 For i = 1 To UBound(d) If d(i) <> z Then l
         * = i - 1 r(j) = a_aggr(a, an, k, l) z = d(i) k = i j = j + 1 End If Next i
         * r(j) = a_aggr(a, an, k, i - 1) ReDim Preserve r(j) r(0) = (a(0) \ 1) + f *
         * 0.001 reduce = r End Function }
         */
    }
}


