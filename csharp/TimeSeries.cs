using System;

namespace Yearly {
    public class TimeSeries {
        public double[][] ts;
        public string[] tags;
        private static double nan = double.NaN;

        public TimeSeries() {
            this.ts = new double[0][];
            this.tags = new string[0];
        }

        public TimeSeries(double[] arg) {
            this.ts = new double[1][];
            this.ts[0] = (arg[0] == -2147483606) ? arg : Vector.cat(-2147483606, arg);
            this.tags = new string[1];
        }

        public TimeSeries(double arg) {
            this.ts = new double[1][];
            this.ts[0] = new double[] { -2147483606.0, arg };
            this.tags = new string[1];
        }

        public TimeSeries(int count) {
            this.ts = new double[count][];
            this.tags = new string[count];
        }

        public TimeSeries(string s) {
            numStr(s.Replace(";", "").Split(','));
        }

        public TimeSeries(string[] s) {
            numStr(s);
        }

        public TimeSeries(string arg1, string arg2) {
            string[] t = arg1.Replace(" ", "").Split(';');
            double[] d;
            this.ts = new double[t.Length][];
            for (int i = 0; i < t.Length; i++) {
                string[] s = t[i].Split(',');
                d = new double[s.Length];
                d[0] = tsHeader(s[0]);
                for (int j = 0; j < s.Length; j++) {
                    d[j] = double.Parse(s[j]);
                }
                this.ts[i] = d;
            }
            t = arg2.Split(',');
            this.tags = new string[t.Length];
            for (int i = 0; i < t.Length; i++) {
                this.tags[i] = t[i].Replace(" ", "");
            }
        }

        private void numStr(string[] s) {
            //	final string[] t = VbaUtil.split(arg.replaceAll(" ", ""), ';');
            double[] d;
            this.ts = new double[1][];
            //		for (int i = 0; i < t.Length; i++) {
            //			final string[] s = VbaUtil.split(data, ',');
            d = new double[s.Length];
            d[0] = tsHeader(s[0]);
            for (int j = 1; j < s.Length; j++) {
                if (s[j] == null) s[j] = "NaN";
                if (!validateDouble(s[j])) s[j] = "NaN";
                d[j] = double.Parse(s[j]);
            }
            this.ts[0] = d;
            //		}
            //		for (int i = 0; i < t.Length; i++) {
            //			t[i] = "s" + i;
            //		}
            this.tags = null;
        }

        public TimeSeries assign(TimeSeries arg) {
            TimeSeries result = this.tsCopy();
            for (int i = 0; i < this.ts.Length; i++) {
                double[] b = arg.ts[i];
                double[] r = new double[b.Length];
                r[0] = b[0];
                for (int j = 1; j < b.Length; j++) {
                    r[j] = b[j];
                }
                result.ts[i] = r;
            }
            return result;
        }

        public TimeSeries movavg(int periods) {
            TimeSeries result = this.tsCopy();
            for (int i = 0; i < this.ts.Length; i++) {
                double[] a = this.ts[i];
                double[] r = new double[a.Length - periods - 1];
                r[0] = Duration.addPer(a[0], periods);
                for (int j = 1; j < a.Length; j++) {
                    r[j] = Duration.avg(a, periods, j);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public TimeSeries sin() {
            TimeSeries result = this.tsCopy();
            for (int i = 0; i < this.ts.Length; i++) {
                double[] a = this.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = Math.Sin(a[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public string toString() {
            string r = "";
            for (int i = 0; i < this.ts.Length; i++) {
                if (this.tags != null && this.tags[i] != "") {
                    r = r + this.tags[i] + ": ";
                }
                r = r + toString(this.ts[i]) + ";";
            }
            return r;
        }

        public double[] intersect(double[] a, double[] b, int idx) {

            int d1, d2, d3, d4, d5;
            int l1, l2;
            int a1;
            int b1, b2;
            int c1, c2;
            int f;
            double g;
            double[] r;

            g = a[0]; //Time element of a
            f = Duration.freq(g);
            if (f != Duration.freq(b[0])) return null;
            g = g - (int)g;

            d1 = Duration.periodNr(a[0]);
            d2 = Duration.periodNr(b[0]);

            l1 = a.Length - 1;
            l2 = b.Length - 1;

            if (d1 == d2 && l1 == l2) return (idx == 0) ? a : b;

            b1 = d1 - 1;
            b2 = d1 + l1 - 1;
            c1 = d2 - 1;
            c2 = d2 + l2 - 1;

            d3 = Math.Max(b1, c1);
            d4 = Math.Min(b2, c2);
            d5 = Math.Max(d1, d2);

            if (d4 < d3 + 1) {
                r = new double[1];
                r[0] = (idx == 0) ? Duration.periodDate(d1, f) + g : Duration.periodDate(d2, f) + g;
                return r;
            }

            r = new double[d4 - d3 + 1];
            a1 = (idx == 0) ? Math.Max(0, d2 - d1) : Math.Max(0, d1 - d2);

            for (int i = 1; i < r.Length; i++) {
                r[i] = (idx == 0) ? a[i + a1] : b[i + a1];
            }

            r[0] = Duration.periodDate(d5, f) + g;
            return r;
        }

        public double[] union(double[] a, double[] b, int idx) {

            int d1, d2, d3, d4, d5;
            int l1, l2;
            int a1;
            int b1, b2;
            int c1, c2;
            int f;
            double g;
            double[] r;

            g = a[0]; //Time element of a
            f = Duration.freq(g);
            if (f != Duration.freq(b[0])) return null;
            g = g - (int)g;

            d1 = Duration.periodNr(a[0]);
            d2 = Duration.periodNr(b[0]);

            l1 = a.Length - 1;
            l2 = b.Length - 1;

            if (d1 == d2 && l1 == l2) return (idx == 0) ? a : b;

            b1 = d1 - 1;
            b2 = d1 + l1 - 1;
            c1 = d2 - 1;
            c2 = d2 + l2 - 1;

            d3 = Math.Min(b1, c1); // earlier of two start dates
            d4 = Math.Max(b2, c2); // later of two end dates
            d5 = Math.Min(d1, d2); // earlier of two start dates

            //  if (d4 < d3 + 1) {
            //  	r = new double[1];
            //  	r[0] = (idx == 0) ? DurationUtil.periodDate(d1, f) + g : DurationUtil.periodDate(d2, f) + g;
            //  	return r;
            //  }

            r = new double[d4 - d3 + 1];
            a1 = (idx == 0) ? d1 - d5 : d2 - d5; // Math.max(0, d2 - d1) : Math.max(0, d1 - d2);

            for (int i = 1; i < r.Length; i++) {
                r[i] = nan;
            }

            if (idx == 0) {
                for (int i = 1; i < a.Length; i++) {
                    r[i + a1] = a[i];
                }
            }

            if (idx == 1) {
                for (int i = 1; i < b.Length; i++) {
                    r[i + a1] = b[i];
                }
            }

            // for (int i = 1; i < r.Length; i++) {
            // 	r[i] = (idx == 0) ? a[i + a1] : b[i + a1];
            // }

            r[0] = Duration.periodDate(d5, f) + g;
            return r;
        }


        private double tsHeader(string arg) {
            return (!arg.Contains(".")) ? Duration.convDate(arg) : double.Parse(arg);
        }

        public string[] toArray() {
            string[] returnArray = new string[this.ts.Length];
            for (int i = 0; i < this.ts.Length; i++) {
                returnArray[i] = string.Join(" ", toArray(i)) + ";" + (i < this.ts.Length - 1 ? "\n" : "");
            }
            return returnArray;
        }

        public string[] toArray(int idx) {
            double[] d = this.ts[idx];
            bool isUndated = d[0] == -2147483606;
            string[] returnArray = new string[d.Length - (isUndated ? 1 : 0)];
            if (!isUndated) returnArray[0] = Duration.reformatDate(d[0]);
            if (isUndated) {
                for (int i = 0; i < d.Length - 1; i++) returnArray[i] = toString(d[i + 1]);
            } else {
                for (int i = 1; i < d.Length; i++) returnArray[i] = toString(d[i]);
            }
            return returnArray;
        }

        private string toString(double d) {
            if (d > int.MaxValue || d < int.MinValue) return d.ToString();
            int i = (int)d;
            return i == d ? i.ToString() : d.ToString();
        }

        private string toString(double[] d) {
            double f = d[0];
            //        string r = (int)f + "." + Duration.freq(f);

            string r = (f == -2147483606) ? "cs" : Duration.reformatDate(f);
            for (int i = 1; i < d.Length; i++) {
                r = r + ((double.IsNaN(d[i])) ? ", NaN" : "," + toString(d[i]));
            }
            return r;
        }

        public static double[] fillMV(double[] a) {
            double[] r = new double[a.Length];
            r[0] = a[0];
            for (int j = 1; j < a.Length; j++) r[j] = nan;
            return r;
        }

        public TimeSeries tsCopy() {
            TimeSeries result = new TimeSeries(this.ts.Length);
            if (this.tags != null) {
                for (int i = 0; i < this.tags.Length; i++) {
                    result.tags[i] = this.tags[i] + "";
                }
            }
            for (int i = 0; i < this.ts.Length; i++) {
                result.ts[i] = (double[])this.ts[i].Clone();
            }
            return result;
        }

        public static bool validateDouble(string arg) {
            char[] c = arg.ToCharArray();
            int r = 0;
            int p = 0;
            int m = 1;
            int n;
            for (int i = c.Length - 1; i > -1; i--) {
                switch (c[i]) {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        n = 1;
                        break;
                    case '.':
                        n = 2;
                        break;
                    case '-':
                        n = 3;
                        break;
                    case 'e':
                    case 'E':
                        n = 4;
                        break;
                    default:
                        return false;
                }

                if (n != 1 || p != 1) {
                    p = n;
                    r = n * m + r;
                    // return false if overflow
                    m *= 10;
                }
            }

            if (r > 3121431) return false;

            if (r == 1 || r == 12 || r == 121 || r == 21) return true; // 1 1. 1.2 .2
            if (r == 31 || r == 312 || r == 3121 || r == 321) return true; // -1 etc.

            if (r == 141 || r == 1241 || r == 12141 || r == 2141) return true; // 1e1 1.e1 1.2e1 .2e1
            if (r == 1431 || r == 12431 || r == 121431 || r == 21431) return true; // 1e-1 1.e-1 1.2e-1 .2e-1

            if (r == 3141 || r == 31241 || r == 312141 || r == 32141) return true; // -1e1 -1.e1 -1.2e1 -.2e1
            if (r == 31431 || r == 312431 || r == 3121431 || r == 321431) return true; // -1e-1 -1.e-1 -1.2e-1 -.2e-1

            return false;
        }

        public static double[] trim(double[] arg) {
            int s, e;
            double[] r;

            // count up nans from the beginning
            s = 0;
            for (int i = 1; i < arg.Length; i++) {
                if (double.IsNaN(arg[i])) s++;
                else break;
            }

            // if all nan, return empty series
            if (s == arg.Length - 1) {
                r = new double[1];
                r[0] = arg[0];
                return r;
            }

            // count up nans from the end
            e = 0;
            for (int i = arg.Length - 1; i > 0; i--) {
                if (double.IsNaN(arg[i])) e++; else break;
            }

            // new timeseries of only what we need
            r = new double[arg.Length - s - e];
            for (int i = 0; i < r.Length - 1; i++) {
                r[i + 1] = arg[s + i + 1];
            }
            r[0] = Duration.addPer(arg[0], s);
            return r;
        }

        public static TimeSeries union(TimeSeries arg) {
            int l = arg.ts.Length;
            double[][] s = new double[l][];
            double[][] z = new double[l][];
            double[] r;
            int[] f = new int[l];
            int b = 2147483647;
            int e = -2147483648;
            int t;
            TimeSeries result;

            // collect timeseries
            for (int i = 0; i < l; i++) {
                s[i] = trim(arg.ts[i]);
                f[i] = Duration.freq(arg.ts[i][0]);
            }

            // check frequencies
            for (int i = 0; i < l; i++) {
                if (f[0] != f[i]) throw new Exception("Mixed frequencies");
            }

            // find the widest series
            for (int i = 0; i < l; i++) {
                t = Duration.periodNr(s[i][0]);
                b = Math.Min(b, t);
                e = Math.Max(e, t + s[i].Length - 1);
            }

            // prototype widest result
            r = new double[e - b + 1];
            r[0] = Duration.tsDate(b, f[0]);
            for (int i = 1; i < r.Length; i++) {
                r[i] = double.NaN; //  -2147483648;
            }

            // copy to padded result
            for (int j = 0; j < l; j++) {
                z[j] = (double[])r.Clone();
                for (int i = 1; i < s[j].Length; i++) {
                    t = Duration.periodNr(s[j][0]);
                    z[j][t - b + i] = s[j][i];
                }
            }

            result = new TimeSeries();
            result.ts = z;
            result.tags = (string[])arg.tags.Clone();
            return result; //  arg;    
        }
    }
}