﻿using System;

namespace Yearly {
    class Arith {
        private static double nan = double.NaN;

        public static bool isNaN(double d) {
            return double.IsNaN(d);
        }

        public static TimeSeries merge(TimeSeries ts, TimeSeries arg, int mvArg, int mult) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    double lt = a2[j];
                    double rt = b2[j];

                    if (isNaN(lt) && isNaN(rt)) {
                        r[j] = nan;
                        continue;
                    }

                    switch (mvArg) {
                        case 1:
                            //!+
                            r[j] = isNaN(lt) ? rt : lt;
                            break;
                        case 2:
                            //+!
                            r[j] = isNaN(rt) ? lt : rt;
                            break;
                        case 3:
                            //!+!
                            if ((isNaN(lt) == false) && (isNaN(rt) == false)) {
                                //If they are both numbers, nan
                                r[j] = nan;
                            } else {
                                r[j] = (isNaN(lt) == false) && (isNaN(rt) == false) ? rt : lt;
                            }
                            break;
                    }
                    //				if(mult < 0 && isNaN(r[j])==false) {
                    r[j] = r[j] * mult;
                    //				}
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries merge(TimeSeries ts, double arg, int mvArg, int mult) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg) && (mvArg == 2 || mvArg == 3)) {
                arg = 0;
            }
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    double lt = a[j];
                    if (isNaN(lt) && (mvArg == 1 || mvArg == 3)) lt = 0;
                    r[j] = isNaN(lt) ? nan : lt - arg;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries merge(double arg, TimeSeries ts, int mvArg, int mult) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg) && (mvArg == 1 || mvArg == 3)) arg = 0;
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    double rt = a[j];
                    if (isNaN(rt) && (mvArg == 2 || mvArg == 3)) rt = 0;
                    r[j] = isNaN(rt) ? nan : arg - rt;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double merge(double arg, double arg2, int mvArg, int mult) {
            if (isNaN(arg) && (mvArg == 1 || mvArg == 3)) arg = 0;
            if (isNaN(arg2) && (mvArg == 2 || mvArg == 3)) arg2 = 0;
            if (isNaN(arg)) return nan;
            if (isNaN(arg2)) return nan;
            return arg - arg2;
        }

        public static TimeSeries minus(TimeSeries ts, TimeSeries arg, int mvArg) {
            // extended minus with extended nan handling
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.union(a, b, 0);
                double[] b2 = ts.union(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    double lt = a2[j];
                    double rt = b2[j];

                    switch (mvArg) {
                        case 0:
                        //+
                        case 1:
                            //#+
                            lt = isNaN(lt) ? 0 : lt;
                            break;
                        case 2:
                            //+#
                            rt = isNaN(rt) ? 0 : rt;
                            break;
                        case 3:
                            //#+#
                            rt = isNaN(rt) ? 0 : rt;
                            lt = isNaN(lt) ? 0 : lt;
                            break;
                    }
                    r[j] = isNaN(lt) || isNaN(rt) ? nan : lt - rt;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries minus(TimeSeries ts, double arg, int mvArg) {
            // extended minus with extended nan handling
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg) && (mvArg == 2 || mvArg == 3)) {
                arg = 0;
            }
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    double lt = a[j];
                    if (isNaN(lt) && (mvArg == 1 || mvArg == 3)) {
                        lt = 0;
                    }
                    r[j] = isNaN(lt) ? nan : lt - arg;
                }
                result.ts[i] = TimeSeries.trim(r);
            }
            return result;
        }

        public static TimeSeries minus(double arg, TimeSeries ts, int mvArg) {
            // extended minus with extended nan handling
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg) && (mvArg == 1 || mvArg == 3)) {
                arg = 0;
            }
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    double rt = a[j];
                    if (isNaN(rt) && (mvArg == 2 || mvArg == 3)) {
                        rt = 0;
                    }
                    r[j] = isNaN(rt) ? nan : arg - rt;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double minus(double arg, double arg2, int mvArg) {
            // extended minus with extended nan handling
            if (isNaN(arg) && (mvArg == 1 || mvArg == 3)) {
                arg = 0;
            }
            if (isNaN(arg2) && (mvArg == 2 || mvArg == 3)) {
                arg2 = 0;
            }
            if (isNaN(arg)) return nan;
            if (isNaN(arg2)) return nan;
            return arg - arg2;
        }

        public static TimeSeries minus(TimeSeries ts, TimeSeries arg) {
            // ordinary minus without extended nan handling
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    double lt = a2[j];
                    double rt = b2[j];
                    r[j] = isNaN(lt) || isNaN(rt) ? nan : lt - rt;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries minus(TimeSeries ts, double arg) {
            // ordinary minus without extended nan handling
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    double lt = a[j];
                    r[j] = isNaN(lt) ? nan : lt - arg;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries minus(double arg, TimeSeries ts) {
            // ordinary minus without extended nan handling
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    double rt = a[j];
                    r[j] = isNaN(rt) ? nan : arg - rt;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double minus(double arg, double arg2) {
            // ordinary minus without extended nan handling
            return arg - arg2;
        }

        public static TimeSeries plus(TimeSeries ts, TimeSeries arg, int mvArg) {
            // extended plus with extended nan handling
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.union(a, b, 0);
                double[] b2 = ts.union(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    double lt = a2[j];
                    double rt = b2[j];

                    switch (mvArg) {
                        case 0:
                        //+
                        case 1:
                            //#+
                            lt = isNaN(lt) ? 0 : lt;
                            break;
                        case 2:
                            //+#
                            rt = double.IsNaN(rt) ? 0 : rt;
                            break;
                        case 3:
                            //#+#
                            rt = isNaN(rt) ? 0 : rt;
                            lt = isNaN(lt) ? 0 : lt;
                            break;
                    }
                    r[j] = isNaN(lt) || isNaN(rt) ? nan : lt + rt;
                }
                result.ts[i] = TimeSeries.trim(r);
            }
            return result;
        }

        public static TimeSeries plus(TimeSeries ts, double arg, int mvArg) {
            // extended plus with extended nan handling
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg) && (mvArg == 2 || mvArg == 3)) {
                arg = 0;
            }

            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    double lt = a[j];
                    if (isNaN(lt) && (mvArg == 1 || mvArg == 3)) {
                        lt = 0;
                    }
                    r[j] = isNaN(lt) ? nan : lt + arg;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries plus(double arg, TimeSeries ts, int mvArg) {
            // extended plus with extended nan handling
            //Flip the mv
            if (mvArg == 2) {
                mvArg = 1;
            } else if (mvArg == 1) {
                mvArg = 2;
            }
            return plus(ts, arg, mvArg);
        }

        public static double plus(double arg, double arg2, int mvArg) {
            // extended plus with extended nan handling
            if (isNaN(arg) && (mvArg == 1 || mvArg == 3)) {
                arg = 0;
            }
            if (isNaN(arg2) && (mvArg == 2 || mvArg == 3)) {
                arg2 = 0;
            }

            //		if (isNaN(arg)) return nan;
            //		if (isNaN(arg2)) return nan;
            return arg + arg2;
        }

        public static TimeSeries plus(TimeSeries ts, TimeSeries arg) {
            // ordinary plus without extended nan handling
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    double lt = a2[j];
                    double rt = b2[j];
                    r[j] = isNaN(lt) || isNaN(rt) ? nan : lt + rt;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries plus(TimeSeries ts, double arg) {
            // ordinary plus without extended nan handling
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    double lt = a[j];
                    r[j] = isNaN(lt) ? nan : lt + arg;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries plus(double arg, TimeSeries ts) {
            // ordinary plus without extended nan handling
            return plus(ts, arg);
        }

        public static double plus(double arg, double arg2) {
            // ordinary plus without extended nan handling
            return arg + arg2;
        }


        public static TimeSeries times(TimeSeries ts, TimeSeries arg) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    r[j] = a2[j] * b2[j];
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries times(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = a[j] * arg;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries times(double arg, TimeSeries ts) {
            return times(ts, arg);
        }

        public static double times(double arg, double arg2) {
            //		if (isNaN(arg)) return nan;
            //		if (isNaN(arg2)) return nan;
            return arg * arg2;
        }

        public static TimeSeries divide(TimeSeries ts, TimeSeries arg) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    r[j] = b2[j] == 0.0 ? nan : a2[j] / b2[j];
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries divide(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg) || arg == 0) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = a[j] / arg;
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries divide(double arg, TimeSeries ts) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = a[j] == 0.0 ? nan : arg / a[j];
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double divide(double arg, double arg2) {
            if (isNaN(arg)) return nan;
            if (isNaN(arg2)) return nan;
            if (arg2 == 0) return nan;
            return arg / arg2;
        }

        public static TimeSeries mod(TimeSeries ts, TimeSeries arg) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    r[j] = ArithUtil.fmod(b2[j], a2[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries mod(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = ArithUtil.fmod(arg, a[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries mod(double arg, TimeSeries ts) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = ArithUtil.fmod(arg, a[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double mod(double arg, double arg2) {
            //		if (isNaN(arg)) return nan;
            //		if (isNaN(arg2)) return nan;
            return ArithUtil.fmod(arg2, arg);
        }

        public static TimeSeries larger(TimeSeries ts, TimeSeries arg) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    r[j] = Math.Max(a2[j], b2[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries larger(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = Math.Max(a[j], arg);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries larger(double arg, TimeSeries ts) {
            return larger(ts, arg);
        }

        public static double larger(double arg, double arg2) {
            //		if (isNaN(arg)) return nan;
            //		if (isNaN(arg2)) return nan;
            return Math.Max(arg, arg2);
        }

        public static TimeSeries smaller(TimeSeries ts, TimeSeries arg) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    r[j] = Math.Min(a2[j], b2[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries smaller(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = Math.Min(a[j], arg);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries smaller(double arg, TimeSeries ts) {
            return Arith.smaller(ts, arg);
        }

        public static double smaller(double arg, double arg2) {
            //		if (isNaN(arg)) return nan;
            //		if (isNaN(arg2)) return nan;
            return Math.Min(arg, arg2);
        }

        public static TimeSeries round(TimeSeries ts, TimeSeries arg) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    r[j] = b2[j] == 0.0 ? nan : ArithUtil.rnd(a2[j], b2[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries round(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg) || arg == 0) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = ArithUtil.rnd(a[j], arg);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries round(double arg, TimeSeries ts) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = a[j] == 0.0 ? nan : ArithUtil.rnd(arg, a[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double round(double arg, double arg2) {
            if (isNaN(arg)) return nan;
            if (isNaN(arg2)) return nan;
            return ArithUtil.rnd(arg, arg2);
        }

        public static TimeSeries log(TimeSeries ts, TimeSeries arg) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    r[j] = b2[j] == 0.0 ? nan : ArithUtil.log(a2[j], b2[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries log(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg) || arg == 0) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = ArithUtil.log(a[j], arg);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries log(double arg, TimeSeries ts) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = a[j] == 0.0 ? nan : ArithUtil.log(arg, a[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double log(double arg, double arg2) {
            if (isNaN(arg)) return nan;
            if (isNaN(arg2)) return nan;
            if (arg2 == 0) return nan;
            return ArithUtil.log(arg, arg2);
        }

        public static TimeSeries power(TimeSeries ts, TimeSeries arg) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    r[j] = Math.Pow(a2[j], b2[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries power(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg) || arg == 0) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = Math.Pow(a[j], arg);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries power(double arg, TimeSeries ts) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = Math.Pow(arg, a[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double power(double arg, double arg2) {
            //		if (isNaN(arg)) return nan;
            //		if (isNaN(arg2)) return nan;
            return Math.Pow(arg, arg2);
        }

        public static TimeSeries nroot(TimeSeries ts, TimeSeries arg) {
            TimeSeries result = ts.tsCopy();
            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] b = arg.ts[i];
                double[] a2 = ts.intersect(a, b, 0);
                double[] b2 = ts.intersect(a, b, 1);
                double[] r = new double[a2.Length];
                r[0] = a2[0];
                for (int j = 1; j < a2.Length; j++) {
                    r[j] = b2[j] == 0.0 ? nan : Math.Pow(a2[j], 1.0 / b2[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries nroot(TimeSeries ts, double arg) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg) || arg == 0) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = Math.Pow(a[j], 1.0 / arg);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries nroot(double arg, TimeSeries ts) {
            TimeSeries result = ts.tsCopy();
            if (isNaN(arg)) {
                for (int i = 0; i < ts.ts.Length; i++) {
                    result.ts[i] = TimeSeries.fillMV(ts.ts[i]);
                }
                return result;
            }

            for (int i = 0; i < ts.ts.Length; i++) {
                double[] a = ts.ts[i];
                double[] r = new double[a.Length];
                r[0] = a[0];
                for (int j = 1; j < a.Length; j++) {
                    r[j] = a[j] == 0.0 ? nan : Math.Pow(arg, 1.0 / a[j]);
                }
                result.ts[i] = r;
            }
            return result;
        }

        public static double nroot(double arg, double arg2) {
            //		if (isNaN(arg)) return nan;
            //		if (isNaN(arg2)) return nan;
            if (arg2 == 0) return nan;
            return Math.Pow(arg, 1.0 / arg2);
        }

    }
}
