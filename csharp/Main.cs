using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Yearly { 

    public class Yearly { 

        public static void Main(string[] args) {
            string data = null;
            Dictionary<string, string> parms = new Dictionary<string, string>();
            for (int i = 0; i < args.Length - 1; i++) {
                string t = args[i];
                if (t.Left(1) == "-") parms.Add(t, args[i + 1]);
            }
            // Z&E port numbers: 2525 3535 4545 5555 6565 7510 8510 9595

            if (parms.ContainsKey("-port")) {
                Grobals.initRepl();

                byte[] bytes = new Byte[1024];
                int port = int.Parse(parms["-port"]);

                // Establish the local endpoint for the socket.  
                // Dns.GetHostName returns the name of the   
                // host running the application.  
                IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
                IPAddress ipAddress = ipHostInfo.AddressList[0];
                IPEndPoint localEndPoint = new IPEndPoint(ipAddress, port);

                // Create a TCP/IP socket.  
                Socket listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                // Bind the socket to the local endpoint and   
                // listen for incoming connections.  
                
                listener.Bind(localEndPoint);
                listener.Listen(10);

                // Start listening for connections.  
                while (true) {
                    Console.WriteLine("Waiting for a connection...");
                    // Program is suspended while waiting for an incoming connection.  
                    Socket handler = listener.Accept();
                    data = null;

                    // An incoming connection needs to be processed.  
                    while (true) {
                        int bytesRec = handler.Receive(bytes);
                        data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                        if (data.IndexOf("<EOF>") > -1) break;
                    }
                    data = data.Left(data.Length - 5);

                    // Show the data on the console.  
                    Console.WriteLine("Text received : {0}", data);

                    // timeseries processing
                    try { data = Exec.exec(data); } catch (Exception e) { data = e.Message; }

                    // Echo the data back to the client.  
                    byte[] msg = Encoding.ASCII.GetBytes(data);

                    handler.Send(msg);
                    Console.WriteLine("Text sent : {0}", data);

                    if (Grobals.quit) {
                        handler.Shutdown(SocketShutdown.Both);
                        handler.Close();
                        return;
                    }
                }

                // } catch (Exception e) {
                //     Console.WriteLine(e.ToString());
                // }

                // Console.WriteLine("\nPress ENTER to continue...");
                // Console.Read();

            } else { 

                Grobals.initRepl();

                Console.WriteLine("Yearly - Version 0.6 - 18 July 2020");

                while (true) {
                    Console.Write("> ");
                    string s = Console.ReadLine();
                    try { s = Exec.exec(s); } catch (Exception e) { s = e.Message; }
                    Console.WriteLine(s);
                    if (Grobals.quit) return;
                }
            } 
        }
    }
}

