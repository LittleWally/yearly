// new

using System;

namespace Yearly {
    public class Union {

        // private static string Union = null;
        public double scalar;
        public TimeSeries series;
        public int contents;
        public Union[] unions;
        public string str;
        public string name;

        public Union() {
            this.contents = 0;
            this.series = null;
            this.scalar = 0.0;
            this.unions = null;
            this.str = "";
            this.name = "";
        }

        public Union(double scalar) {
            this.contents = 1;
            this.series = null;
            this.scalar = scalar;
            this.unions = null;
            this.str = "";
            this.name = "";
        }

        public Union(string arg) {
            this.contents = 2;
            this.series = new TimeSeries(arg);
            this.scalar = 0.0;
            this.unions = null;
            this.str = "";
            this.name = "";
        }

        public Union(TimeSeries arg) {
            this.contents = 2;
            this.series = arg;
            this.scalar = 0.0;
            this.unions = null;
            this.str = "";
            this.name = "";
        }

        public Union(Union[] arg) {
            this.contents = 42;
            this.unions = arg;
            this.series = null;
            this.scalar = 0.0;
            this.str = "";
            this.name = "";
        }

        public Union(string arg, int type) {
            this.contents = type;
            this.series = null;
            this.unions = null;
            this.scalar = 0.0;
            this.name = (type == -10) ? arg : "";
            this.str = (type == -20) ? arg : "";
        }

        public TimeSeries getSeries() {
            return series;
        }

        public double getScalar() {
            return scalar;
        }

        public string toString() {
            switch (this.contents) {
                case 1: return "" + this.scalar;
                case 2: return this.series.toString();
                default: return null;
            }
        }

        // extended nan handing for +# #+ #+#
        public static Union plus(Union a, Union b, int mvArg) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Arith.plus(a.scalar, b.scalar, mvArg));
                case 4: return new Union(Arith.plus(a.scalar, b.series, mvArg));
                case 5: return new Union(Arith.plus(a.series, b.scalar, mvArg));
                case 6: return new Union(Arith.plus(a.series, b.series, mvArg));
                default: return null;
            }
        }

        // extended nan handing for -# #- #-#
        public static Union minus(Union a, Union b, int mvArg) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Arith.minus(a.scalar, b.scalar, mvArg));
                case 4: return new Union(Arith.minus(a.scalar, b.series, mvArg));
                case 5: return new Union(Arith.minus(a.series, b.scalar, mvArg));
                case 6: return new Union(Arith.minus(a.series, b.series, mvArg));
                default: return null;
            }
        }

        // no extended nan handing for +
        public static Union plus(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Arith.plus(a.scalar, b.scalar));
                case 4: return new Union(Arith.plus(a.scalar, b.series));
                case 5: return new Union(Arith.plus(a.series, b.scalar));
                case 6: return new Union(Arith.plus(a.series, b.series));
                default: return null;
            }
        }

        // no extended nan handing for -
        public static Union minus(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Arith.minus(a.scalar, b.scalar));
                case 4: return new Union(Arith.minus(a.scalar, b.series));
                case 5: return new Union(Arith.minus(a.series, b.scalar));
                case 6: return new Union(Arith.minus(a.series, b.series));
                default: return null;
            }
        }

        public static Union merge(Union a, Union b, int mvArg, int mult) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Arith.merge(a.scalar, b.scalar, mvArg, mult));
                case 4: return new Union(Arith.merge(a.scalar, b.series, mvArg, mult));
                case 5: return new Union(Arith.merge(a.series, b.scalar, mvArg, mult));
                case 6: return new Union(Arith.merge(a.series, b.series, mvArg, mult));
                default: return null;
            }
        }

        public static Union times(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Arith.times(a.scalar, b.scalar));
                case 4: return new Union(Arith.times(a.scalar, b.series));
                case 5: return new Union(Arith.times(a.series, b.scalar));
                case 6: return new Union(Arith.times(a.series, b.series));
                default: return null;
            }
        }

        public static Union divide(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Arith.divide(a.scalar, b.scalar));
                case 4: return new Union(Arith.divide(a.scalar, b.series));
                case 5: return new Union(Arith.divide(a.series, b.scalar));
                case 6: return new Union(Arith.divide(a.series, b.series));
                default: return null;
            }
        }

        public static Union eq(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Logical.eq(a.scalar, b.scalar));
                case 4: return new Union(Logical.eq(a.scalar, b.series));
                case 5: return new Union(Logical.eq(a.series, b.scalar));
                case 6: return new Union(Logical.eq(a.series, b.series));
                default: return null;
            }
        }

        public static Union ne(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Logical.ne(a.scalar, b.scalar));
                case 4: return new Union(Logical.ne(a.scalar, b.series));
                case 5: return new Union(Logical.ne(a.series, b.scalar));
                case 6: return new Union(Logical.ne(a.series, b.series));
                default: return null;
            }
        }

        public static Union gt(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Logical.gt(a.scalar, b.scalar));
                case 4: return new Union(Logical.gt(a.scalar, b.series));
                case 5: return new Union(Logical.gt(a.series, b.scalar));
                case 6: return new Union(Logical.gt(a.series, b.series));
                default: return null;
            }
        }

        public static Union ge(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Logical.ge(a.scalar, b.scalar));
                case 4: return new Union(Logical.ge(a.scalar, b.series));
                case 5: return new Union(Logical.ge(a.series, b.scalar));
                case 6: return new Union(Logical.ge(a.series, b.series));
                default: return null;
            }
        }

        public static Union lt(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Logical.lt(a.scalar, b.scalar));
                case 4: return new Union(Logical.lt(a.scalar, b.series));
                case 5: return new Union(Logical.lt(a.series, b.scalar));
                case 6: return new Union(Logical.lt(a.series, b.series));
                default: return null;
            }
        }

        public static Union le(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Logical.le(a.scalar, b.scalar));
                case 4: return new Union(Logical.le(a.scalar, b.series));
                case 5: return new Union(Logical.le(a.series, b.scalar));
                case 6: return new Union(Logical.le(a.series, b.series));
                default: return null;
            }
        }

        public static Union and(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Logical.and(a.scalar, b.scalar));
                case 4: return new Union(Logical.and(a.scalar, b.series));
                case 5: return new Union(Logical.and(a.series, b.scalar));
                case 6: return new Union(Logical.and(a.series, b.series));
                default: return null;
            }
        }

        public static Union or(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Logical.or(a.scalar, b.scalar));
                case 4: return new Union(Logical.or(a.scalar, b.series));
                case 5: return new Union(Logical.or(a.series, b.scalar));
                case 6: return new Union(Logical.or(a.series, b.series));
                default: return null;
            }
        }

        public static Union negate(Union a) {
            switch (a.contents) {
                case 1: return new Union(Unary.negate(a.scalar));
                case 2: return new Union(Unary.negate(a.series));
                default: return null;
            }
        }

        public static Union not(Union a) {
            switch (a.contents) {
                case 1: return new Union(Logical.not(a.scalar));
                case 2: return new Union(Logical.not(a.series));
                default: return null;
            }
        }

        public static Union abs(Union a) {
            switch (a.contents) {
                case 1: return new Union(Unary.abs(a.scalar));
                case 2: return new Union(Unary.abs(a.series));
                default: return null;
            }
        }

        public static Union mod(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Arith.mod(a.scalar, b.scalar));
                case 4: return new Union(Arith.mod(a.scalar, b.series));
                case 5: return new Union(Arith.mod(a.series, b.scalar));
                case 6: return new Union(Arith.mod(a.series, b.series));
                default: return null;
            }
        }

        public static Union smaller(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Arith.smaller(a.scalar, b.scalar));
                case 4: return new Union(Arith.smaller(a.scalar, b.series));
                case 5: return new Union(Arith.smaller(a.series, b.scalar));
                case 6: return new Union(Arith.smaller(a.series, b.series));
                default: return null;
            }
        }

        public static Union larger(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Arith.larger(a.scalar, b.scalar));
                case 4: return new Union(Arith.larger(a.scalar, b.series));
                case 5: return new Union(Arith.larger(a.series, b.scalar));
                case 6: return new Union(Arith.larger(a.series, b.series));
                default: return null;
            }
        }

        public static Union trunc(Union a) {
            switch (a.contents) {
                case 1: return new Union(Unary.trunc(a.scalar));
                case 2: return new Union(Unary.trunc(a.series));
                default: return null;
            }
        }

        public static Union exp(Union a) {
            switch (a.contents) {
                case 1: return new Union(Unary.exp(a.scalar));
                case 2: return new Union(Unary.exp(a.series));
                default: return null;
            }
        }

        public static Union ln(Union a) {
            switch (a.contents) {
                case 1: return new Union(Unary.ln(a.scalar));
                case 2: return new Union(Unary.ln(a.series));
                default: return null;
            }
        }

        public static Union sin(Union a) {
            switch (a.contents) {
                case 1: return new Union(Unary.sin(a.scalar));
                case 2: return new Union(Unary.sin(a.series));
                default: return null;
            }
        }

        public static Union cos(Union a) {
            switch (a.contents) {
                case 1: return new Union(Unary.cos(a.scalar));
                case 2: return new Union(Unary.cos(a.series));
                default: return null;
            }
        }

        public static Union tan(Union a) {
            switch (a.contents) {
                case 1: return new Union(Unary.tan(a.scalar));
                case 2: return new Union(Unary.tan(a.series));
                default: return null;
            }
        }

        public static Union round(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Arith.round(a.scalar, b.scalar));
                case 4: return new Union(Arith.round(a.scalar, b.series));
                case 5: return new Union(Arith.round(a.series, b.scalar));
                case 6: return new Union(Arith.round(a.series, b.series));
                default: return null;
            }
        }

        public static Union log(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Arith.log(a.scalar, b.scalar));
                case 4: return new Union(Arith.log(a.scalar, b.series));
                case 5: return new Union(Arith.log(a.series, b.scalar));
                case 6: return new Union(Arith.log(a.series, b.series));
                default: return null;
            }
        }

        public static Union power(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Arith.power(a.scalar, b.scalar));
                case 4: return new Union(Arith.power(a.scalar, b.series));
                case 5: return new Union(Arith.power(a.series, b.scalar));
                case 6: return new Union(Arith.power(a.series, b.series));
                default: return null;
            }
        }

        public static Union nroot(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Arith.nroot(a.scalar, b.scalar));
                case 4: return new Union(Arith.nroot(a.scalar, b.series));
                case 5: return new Union(Arith.nroot(a.series, b.scalar));
                case 6: return new Union(Arith.nroot(a.series, b.series));
                default: return null;
            }
        }

        public static Union repmv(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Missing.repmv(a.scalar, b.scalar));
                case 4: return new Union(Missing.repmv(a.scalar, b.series));
                case 5: // return new Union(TSMUtil.repmv(a.series, b.scalar));
                case 6: // return new Union(TSMUtil.repmv(a.series, b.series));
                default: return null;
            }
        }

        public static Union repmv(Union b) {
            switch (b.contents) {
                case 1: return new Union(Missing.repmv(0, b.scalar));
                case 2: return new Union(Missing.repmv(0, b.series));
                default: return null;
            }
        }

        public static Union idx(Union a, Union b) {

            if (b.contents == -20) {
                return new Union(Aggr.index(a.series, b.str));
            }

            switch (a.contents * 2 + b.contents) {
                case 3: return null;
                case 4: return null;
                case 5: return new Union(Aggr.index(a.series, b.scalar));
                case 6: return null;
            }
            return null;
        }

        public static Union idx(Union a, Union b, Union c) {
            return null;
        }

        public static Union sum(Union a) {
            switch (a.contents) {
                case 1: return new Union(Aggr.sum(a.scalar));
                case 2: return new Union(Aggr.sum(a.series));
                default: return null;
            }
        }

        public static Union avg(Union a) {
            switch (a.contents) {
                case 1: return new Union(Aggr.avg(a.scalar));
                case 2: return new Union(Aggr.avg(a.series));
                default: return null;
            }
        }

        public static Union gmean(Union a) {
            switch (a.contents) {
                case 1: return new Union(Aggr.gmean(a.scalar));
                case 2: return new Union(Aggr.gmean(a.series));
                default: return null;
            }
        }

        public static Union hmean(Union a) {
            switch (a.contents) {
                case 1: return new Union(Aggr.hmean(a.scalar));
                case 2: return new Union(Aggr.hmean(a.series));
                default: return null;
            }
        }

        public static Union min(Union a) {
            switch (a.contents) {
                case 1: return new Union(Aggr.min(a.scalar));
                case 2: return new Union(Aggr.min(a.series));
                default: return null;
            }
        }

        public static Union max(Union a) {
            switch (a.contents) {
                case 1: return new Union(Aggr.max(a.scalar));
                case 2: return new Union(Aggr.max(a.series));
                default: return null;
            }
        }

        public static Union stdev(Union a) {
            switch (a.contents) {
                case 1: return new Union(Aggr.stdev(a.scalar));
                case 2: return new Union(Aggr.stdev(a.series));
                default: return null;
            }
        }

        public static Union all(Union a) {
            switch (a.contents) {
                case 1: return new Union(Aggr.all(a.scalar));
                case 2: return new Union(Aggr.all(a.series));
                default: return null;
            }
        }

        public static Union any(Union a) {
            switch (a.contents) {
                case 1: return new Union(Aggr.any(a.scalar));
                case 2: return new Union(Aggr.any(a.series));
                default: return null;
            }
        }

        public static Union movavg(Union a, Union b) {
            switch (a.contents) {
                case 1: return new Union(Aggr.movavg(a.scalar, b.scalar));
                case 2: return new Union(Aggr.movavg(a.series, b.scalar));
                default: return null;
            }
        }

        public static Union pch(Union a) {
            switch (a.contents) {
                case 1: return new Union(Shift.pch(a.scalar, 1));
                case 2: return new Union(Shift.pch(a.series, 1));
                default: return null;
            }
        }

        public static Union pch(Union a, Union b) {
            switch (a.contents) {
                case 1: return new Union(Shift.pch(a.scalar, b.scalar));
                case 2: return new Union(Shift.pch(a.series, b.scalar));
                default: return null;
            }
        }

        public static Union scsv(Union a) {
            switch (a.contents) {
                case 1: return new Union(Aggr.scalar(a.scalar));
                case 2: return new Union(Aggr.scalar(a.series));
                default: return null;
            }
        }

        public static Union lag(Union a, Union b) {
            switch (a.contents) {
                case 1: return new Union(Shift.lag(a.scalar, b.scalar));
                case 2: return new Union(Shift.lag(a.series, b.scalar));
                default: return null;
            }
        }

        public static Union daily(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(TimeSeriesConstructors.daily(a.scalar, b.scalar));
                case 4: return new Union(TimeSeriesConstructors.daily(a.scalar, b.series));
                case 5: // return new Union(missing.repmv(a.series, b.scalar));
                case 6: // return new Union(missing.repmv(a.series, b.series));
                default: return null;
            }
        }

        public static Union weekly(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(TimeSeriesConstructors.weekly(a.scalar, b.scalar));
                case 4: return new Union(TimeSeriesConstructors.weekly(a.scalar, b.series));
                case 5: // return new Union(missing.repmv(a.series, b.scalar));
                case 6: // return new Union(missing.repmv(a.series, b.series));
                default: return null;
            }
        }

        public static Union monthly(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(TimeSeriesConstructors.monthly(a.scalar, b.scalar));
                case 4: return new Union(TimeSeriesConstructors.monthly(a.scalar, b.series));
                case 5: // return new Union(missing.repmv(a.series, b.scalar));
                case 6: // return new Union(missing.repmv(a.series, b.series));
                default: return null;
            }
        }

        public static Union quarterly(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(TimeSeriesConstructors.quarterly(a.scalar, b.scalar));
                case 4: return new Union(TimeSeriesConstructors.quarterly(a.scalar, b.series));
                case 5: // return new Union(missing.repmv(a.series, b.scalar));
                case 6: // return new Union(missing.repmv(a.series, b.series));
                default: return null;
            }
        }

        public static Union annual(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(TimeSeriesConstructors.annual(a.scalar, b.scalar));
                case 4: return new Union(TimeSeriesConstructors.annual(a.scalar, b.series));
                case 5: // return new Union(missing.repmv(a.series, b.scalar));
                case 6: // return new Union(missing.repmv(a.series, b.series));
                default: return null;
            }
        }

        public static Union assign(Union x, Union y) {
            Grobals.vars.Remove(x.name);
            Grobals.vars[x.name] = y;
            return y;
        }

        public static Union join(Union a, Union b) {
            switch (a.contents) {
                case 1: return a;
                case 2: return new Union(TimeSeriesConstructors.join(a.series, b.series));
                default: return null;
            }
        }

        public static Union compressQuarterly(Union a, Union b) {
            switch (a.contents) {
                case 1: return a;
                case 2: return new Union(ChangeFrequency.compressQuarterly(a.series, b.str));
                default: return null;
            }
        }

        public static Union compressAnnual(Union a, Union b) {
            switch (a.contents) {
                case 1: return a;
                case 2: return new Union(ChangeFrequency.compressAnnual(a.series, b.str));
                default: return null;
            }
        }

        public static Union compressWeekly(Union a, Union b) {
            switch (a.contents) {
                case 1: return a;
                case 2: return new Union(ChangeFrequency.compressWeekly(a.series, b.str));
                default: return null;
            }
        }

        public static Union compressMonthly(Union a, Union b) {
            switch (a.contents) {
                case 1: return a;
                case 2: return new Union(ChangeFrequency.compressMonthly(a.series, b.str));
                default: return null;
            }
        }

        public static Union expandQuarterly(Union a, Union b) {
            switch (a.contents) {
                case 1: return a;
                case 2: return new Union(ChangeFrequency.expandQuarterly(a.series, b.str));
                default: return null;
            }
        }

        public static Union expandMonthly(Union a, Union b) {
            switch (a.contents) {
                case 1: return a;
                case 2: return new Union(ChangeFrequency.expandMonthly(a.series, b.str));
                default: return null;
            }
        }

        public static Union expandWeekly(Union a, Union b) {
            switch (a.contents) {
                case 1: return a;
                case 2: return new Union(ChangeFrequency.expandWeekly(a.series, b.str));
                default: return null;
            }
        }

        public static Union expandDaily(Union a, Union b) {
            switch (a.contents) {
                case 1: return a;
                case 2: return new Union(ChangeFrequency.expandDaily(a.series, b.str));
                default: return null;
            }
        }

        public static Union get(Union a) {
            if (a.contents != -20) throw new Exception("Incorrect get argument " + a.contents);
            string[] s = a.str.Split('.');
            if (s.Length != 2) throw new Exception("Incorrect get argument " + a.str);
            string answer = ""; // Globals.ThisAddIn.Application.Run("loader." + s[0] + "Series", s[1]);
            return new Union(answer);
        }

        public static Union concat(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Vector.concat(a.scalar, b.scalar));
                case 4: //return new Union(Vector.concat(a.scalar, b.series));
                case 5: //return new Union(Vector.concat(a.series, b.scalar));
                case 6: //return new Union(Vector.concat(a.series, b.series));
                default: return null;
            }
        }

        public static Union conditional(Union a, Union b, Union c) {
            if (a.contents != 1) throw new Exception("conditional not scalar");
            if (a.scalar == 0) return c;
            else return b;
        }

        public static Union miota(Union a, Union b) {
            switch (a.contents * 2 + b.contents) {
                case 3: return new Union(Vector.miota(a.scalar, b.scalar));
                default: return null;
            }
        }
    }
}
