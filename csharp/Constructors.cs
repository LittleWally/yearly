﻿using System;

namespace Yearly {
    class TimeSeriesConstructors {

        public static TimeSeries daily(double s, TimeSeries arg) {
            TimeSeries result = arg.tsCopy();
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] b = arg.ts[i];
                double[] r = (double[])b.Clone();
                r[0] = s + 0.366;
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries daily(double s, double arg) {
            TimeSeries result = new TimeSeries(arg);
            result.ts[0][0] = s + 0.366;
            return result;
        }

        public static TimeSeries weekly(double s, TimeSeries arg) {
            TimeSeries result = arg.tsCopy();
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] b = arg.ts[i];
                double[] r = (double[])b.Clone();
                r[0] = s + 0.053;
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries weekly(double s, double arg) {
            TimeSeries result = new TimeSeries(arg);
            result.ts[0][0] = s + 0.053;
            return result;
        }

        public static TimeSeries monthly(double s, TimeSeries arg) {
            TimeSeries result = arg.tsCopy();
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] b = arg.ts[i];
                double[] r = (double[])b.Clone();
                r[0] = s + 0.012;
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries monthly(double s, double arg) {
            TimeSeries result = new TimeSeries(arg);
            result.ts[0][0] = s + 0.012;
            return result;
        }

        public static TimeSeries quarterly(double s, TimeSeries arg) {
            TimeSeries result = arg.tsCopy();
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] b = arg.ts[i];
                double[] r = (double[])b.Clone();
                r[0] = s + 0.004;
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries quarterly(double s, double arg) {
            TimeSeries result = new TimeSeries(arg);
            result.ts[0][0] = s + 0.004;
            return result;
        }

        public static TimeSeries annual(double s, TimeSeries arg) {
            TimeSeries result = arg.tsCopy();
            for (int i = 0; i < arg.ts.Length; i++) {
                double[] b = arg.ts[i];
                double[] r = (double[])b.Clone();
                r[0] = s + 0.001;
                result.ts[i] = r;
            }
            return result;
        }

        public static TimeSeries annual(double s, double arg) {
            TimeSeries result = new TimeSeries(arg);
            result.ts[0][0] = s + 0.001;
            return result;
        }

        public static TimeSeries join(TimeSeries a, TimeSeries b) {
            TimeSeries result = new TimeSeries(a.ts.Length + b.ts.Length);
            int c = 0;
            for (int i = 0; i < a.ts.Length; i++) {
                result.ts[c] = (double[])a.ts[i].Clone();
                // result.tags[c] = a.tags[i] + "";
                c++;
            }
            for (int i = 0; i < b.ts.Length; i++) {
                result.ts[c] = (double[])b.ts[i].Clone();
                // result.tags[c](double[]) = b.tags[i] + "";
                c++;
            }
            return result;
        }
    }
}
